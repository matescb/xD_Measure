from xdCore import stg, cmg, spm, saveMeasured
import numpy as np
import threading
import time

class Scanner(object):

    class LiveThread(threading.Thread):
        def __init__(self,scanner):
            threading.Thread.__init__(self)
            self._stop_event = threading.Event()
            self.scanner=scanner

        def stop(self):
            self._stop_event.set()

        def stopped(self):
            return self._stop_event.is_set()

        def run(self):
            if not self._stop_event.is_set():
                self.scanner._lin.go_move(self.scanner._start)
                self.scanner._lin.wait_for_stop()

            for position in np.arange(self.scanner._start, self.scanner._stop+self.scanner._step, self.scanner._step):
                if not self._stop_event.is_set():
                    self.scanner._lin.go_move(position)

                    spm.measureAll()
                    cmg.measureAll()

                    while len(list(spm._th))>0:
                        time.sleep(1)

                    if self.scanner._namingConvention=="Timestamps":
                        saveMeasured(self.scanner._folderName)
                    if self.scanner._namingConvention=="Position":
                        name=self.scanner._lin.name+f"_{float(self.scanner._lin.position):08.3f}"
                        saveMeasured(self.scanner._folderName, name)

                    self.scanner._lin.wait_for_stop()

    def __init__(self,stage, start, stop, step, folderName, namingConvention):
        self._lin = stg.getHandler(stage)
        self._start=start
        self._stop=stop
        self._step=step
        self._folderName=folderName
        self._namingConvention=namingConvention

    def start(self):
        self._th= self.LiveThread(self)
        self._th.start()

    def stop(self):
        self._th.stop()

