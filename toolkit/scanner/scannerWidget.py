from PyQt6.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QDialog, QFrame, QSizePolicy
from PyQt6.QtGui import QIcon, QPixmap
from PyQt6 import QtCore, QtWidgets
import sys, os
from xdCore import stg, cmg, spm, saveMeasured

if __name__ == "__main__":
    from scanner_ui import Ui_Scanner
    from scanner import Scanner
else:
    from .scanner_ui import Ui_Scanner
    from .scanner import Scanner

'''
D:\DP\QT_playing\venv\Scripts\pyuic6.exe D:\DP\QT_playing\toolkit\scanner\scanner_ui.ui -o D:\DP\QT_playing\toolkit\scanner\scanner_ui.py
'''


class ToolScanner(Ui_Scanner, QWidget):
    def __init__(self,window, parent=None):
        super(ToolScanner, self).__init__(parent)
        self.setupUi(self)
        self.pushButton_emergencyStop.clicked.connect(stg.go_emergencyStopAll)
        self.pushButton_emergencyStop.clicked.connect(self.buttonStop)
        self.comboBox_stage.addItems(list(stg.stagesOpened))
        self.pushButton_start.clicked.connect(self.buttonStart)
        self.pushButton_stop.clicked.connect(self.buttonStop)

    def buttonStart(self):
        self.scanner = Scanner(self.comboBox_stage.currentText(),self.doubleSpinBox_begin.value(),
                               self.doubleSpinBox_end.value(),self.doubleSpinBox_step.value(), self.lineEdit_scanName.text(), self.comboBox_namingConvention.currentText())
        self.scanner.start()

    def buttonStop(self):
        self.scanner.stop()

if __name__ == '__main__':
    lin = stg.open("HamamatsuStage")
    app = QApplication([])
    window = ToolScanner(app)

    window.show()
    sys.exit(app.exec())