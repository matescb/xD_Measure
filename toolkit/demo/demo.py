from xdCore import stg, cmg, spm, saveMeasured, cfg
import numpy as np
import threading
import time



import numpy as np
import glob
from astropy.io import fits
import shutil
from time import asctime
from os import listdir
import numpy as np
import matplotlib.pyplot as plt

import matplotlib as ml

from scipy.optimize import curve_fit
from scipy import special
import pandas as pd
from scipy.interpolate import UnivariateSpline
import pylab as plt
import os

class Demo(object):

    class LiveThread(threading.Thread):
        def __init__(self,scanner):
            threading.Thread.__init__(self)
            self._stop_event = threading.Event()
            self.scanner=scanner

        def stop(self):
            self._stop_event.set()

        def stopped(self):
            return self._stop_event.is_set()

        def run(self):
            if not self._stop_event.is_set():
                self.scanner._lin.go_move(self.scanner._start)
                self.scanner._lin.wait_for_stop()

            for position in np.arange(self.scanner._start, self.scanner._stop+self.scanner._step, self.scanner._step):
                if not self._stop_event.is_set():
                    self.scanner._lin.go_move(position)

                    spm.measureAll()
                    cmg.measureAll()

                    while len(list(spm._th))>0:
                        time.sleep(1)

                    if self.scanner._namingConvention=="Timestamps":
                        saveMeasured(self.scanner._folderName)
                    if self.scanner._namingConvention=="Position":
                        name=self.scanner._lin.name+f"_{float(self.scanner._lin.position):08.3f}"
                        saveMeasured(self.scanner._folderName, name)

                    self.scanner._lin.wait_for_stop()

            self.scanner.postProcessing()

    def __init__(self,stage, start, stop, step, folderName, namingConvention):
        self._lin = stg.getHandler(stage)
        self._start=start
        self._stop=stop
        self._step=step
        self._folderName=folderName
        self._namingConvention=namingConvention

        self._path = os.path.join(cfg["Project"]["projectFolder"], self._folderName)

    def start(self):
        self._th= self.LiveThread(self)
        self._th.start()

    def stop(self):
        self._th.stop()

    def plot(self, data, name, vmax, x0, x1):
        size_x = len(data) * 0.014
        size_y = len(data[0]) * 0.014
        extent = [x0, x1, 0, size_y]

        plt.clf()
        plt.imshow(data, extent=extent, cmap="viridis", norm=ml.colors.Normalize(vmin=0, vmax=vmax))
        plt.xlabel("Vzdálenost od optiky [mm]")
        plt.ylabel("Výška senzoru [mm]")
        plt.savefig(f"{self._path}/img_{self._folderName}.png", bbox_inches='tight', format="png", dpi=800, transparent=False)
        plt.clf()

    def postProcessing(self):

        al = {}
        for file in glob.glob(f"{self._path}/*/Hamamatsu.fits"):
            hdul = fits.open(file)[0]
            data = hdul.data
            data_axy = data.mean(axis=1)
            pos = float(hdul.header["POSITION"])
            al[f"{pos:08.3f}"] = data

        app = np.zeros([2068, 1])
        for a in al:
            app = np.append(app, al[a].transpose(), axis=1)

        self.plot(np.rot90(app, 2), "DP", app.max(), self._stop, self._start)
       # hdu = fits.PrimaryHDU((app).astype(int))
       # hdu.writeto(f"{self._path}/scan.fits", overwrite=True)


        d = []
        for file in glob.glob(f"{self._path}/*/Hamamatsu.fits"):
            # plt.close('all')
            hdul = fits.open(file)[0]
            data = hdul.data.transpose()
            data_axy = data.mean(axis=1)

            pos = float(hdul.header["POSITION"])
            roi = data_axy  # [int(5/px_size):int(20/px_size)]
            x = np.arange(0, len(roi) * 0.014, 0.014)
            y = np.copy(roi)

            try:
                print(f"NO-{pos:8.2f}")
                row = data_axy

                def make_norm_dist(x, mean, sd):
                    return 1.0 / (sd * np.sqrt(2 * np.pi)) * np.exp(-(x - mean) ** 2 / (2 * sd ** 2))

                #  x = range(0,len(row))
                blue = row
                # create a spline of x and blue-np.max(blue)/2
                spline = UnivariateSpline(x, blue - np.max(blue) / 2, s=0)

                fwhm = 0
                for a in range(1, len(spline.roots()), 2):
                    if spline.roots()[a] - spline.roots()[a - 1] > fwhm:
                        r1 = spline.roots()[a - 1]
                        r2 = spline.roots()[a]
                        fwhm = r2 - r1

                d.append({"File": f"{pos:8.2f}", "FWHM": fwhm})
                print(f"OK-{pos:8.2f}, {fwhm}")

            except:
                print(f"NO-{pos:8.2f}, {fwhm}")


        df = pd.DataFrame(d)[:len(d) - 1]
        df["File"] = df["File"].astype(float)
        plt.clf()
        plt.plot(df["File"], df["FWHM"])
        plt.xlabel("Pozice [mm]")
        plt.ylabel("FWHM [mm]")
        plt.grid()

        pos = df.at[df["FWHM"].idxmin(), "File"]
        val = df.at[df["FWHM"].idxmin(), "FWHM"]

        plt.title(label=f"Minimální FWHM je {val:0.2f} mm na pozici {pos:0.2f} mm")
        plt.axvline(x=df.at[df["FWHM"].idxmin(), "File"], ls='--', color="r", label="Minimální FWHM")
        plt.legend()

        plt.savefig(f"{self._path}/Ohnisko.png", bbox_inches='tight', format="png", dpi=300, transparent=False)

        d = []

        os.mkdir(f"{self._path}/FWHM")
        for file in glob.glob(f"{self._path}/*/Hamamatsu.fits"):
            # plt.close('all')
            hdul = fits.open(file)[0]
            data = hdul.data.transpose()
            data_axy = data.mean(axis=1)

            pos = float(hdul.header["POSITION"])

            # x = np.arange(0, px*px_size, px_size)
            roi = data_axy  # [int(5/px_size):int(20/px_size)]
            x = np.arange(0, len(roi) * 0.014, 0.014)
            y = np.copy(roi)

            try:
                plt.clf()
                # show plot
                plt.figure(4)
                #    plt.plot(x, y)
                # plt.plot(x, y, '-', linewidth=0.5, alpha=0.9)
                row = data_axy

                def make_norm_dist(x, mean, sd):
                    return 1.0 / (sd * np.sqrt(2 * np.pi)) * np.exp(-(x - mean) ** 2 / (2 * sd ** 2))

                #  x = range(0,len(row))
                blue = row
                # create a spline of x and blue-np.max(blue)/2
                spline = UnivariateSpline(x, blue - np.max(blue) / 2, s=0)

                import pylab as pl
                plt.plot(x, y, '-', label=f"Naměřeno", alpha=1)

                fwhm = 0
                for a in range(1, len(spline.roots()), 2):
                    if spline.roots()[a] - spline.roots()[a - 1] > fwhm:
                        r1 = spline.roots()[a - 1]
                        r2 = spline.roots()[a]
                        fwhm = r2 - r1

                plt.axvspan(r1, r2, color="g", alpha=0.5, label=f"FWHM={(r2 - r1):.2f} mm")

                plt.xlabel('Podélná osa senzoru [mm]')
                plt.ylabel('Intenzita [-]')
                # plt.title(file.replace('.fits',''))
                plt.legend()
                plt.title(f"Ohnisko f={pos:3.2f} mm")
                plt.grid('on')

                plt.savefig(f"{self._path}/FWHM/{pos:8.2f}_FWHM.png", bbox_inches='tight', format="png", dpi=100,
                            transparent=False)
                print(f"OK-{pos:8.2f}, {fwhm}")
                plt.clf()

            except:
                print(f"NO-{pos:8.2f}")
        print("Demo done")



#        pd.DataFrame(d).to_excel(f"{self._path}/FWHM.xlsx")

