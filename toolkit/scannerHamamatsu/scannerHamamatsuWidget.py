from PyQt6.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QDialog, QFrame, QSizePolicy
from PyQt6.QtGui import QIcon, QPixmap
from PyQt6 import QtCore, QtWidgets
import sys, os
from xdCore import stg, cmg, spm, saveMeasured

if __name__ == "__main__":
    from scannerHamamatsu_ui import Ui_Scanner
    from scannerHamamatsu import ScannerHamamatsu
else:
    from .scannerHamamatsu_ui import Ui_Scanner
    from .scannerHamamatsu import ScannerHamamatsu

'''
D:\DP\QT_playing\venv\Scripts\pyuic6.exe D:\DP\QT_playing\toolkit\scannerHamamatsu\scannerHamamatsu_ui.ui -o D:\DP\QT_playing\toolkit\scannerHamamatsu\scannerHamamatsu_ui.py
'''


class ToolScannerHamamatsu(Ui_Scanner, QWidget):
    def __init__(self,window, parent=None):
        super(ToolScannerHamamatsu, self).__init__(parent)
        self.setupUi(self)
        self.pushButton_emergencyStop.clicked.connect(stg.go_emergencyStopAll)
        self.pushButton_emergencyStop.clicked.connect(self.buttonStop)
        self.comboBox_stage.addItems(list(stg.stagesOpened))
        self.pushButton_start.clicked.connect(self.buttonStart)
        self.pushButton_stop.clicked.connect(self.buttonStop)

    def buttonStart(self):
        self.scanner = ScannerHamamatsu(self.comboBox_stage.currentText(),self.doubleSpinBox_begin.value(),
                               self.doubleSpinBox_end.value(), self.lineEdit_scanName.text())
    def buttonStop(self):
        self.scanner.stop()

if __name__ == '__main__':
    lin = stg.open("HamamatsuStage")
    app = QApplication([])
    window = ToolScannerHamamatsu(app)

    window.show()
    sys.exit(app.exec())