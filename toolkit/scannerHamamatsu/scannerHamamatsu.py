from xdCore import stg, cmg, spm, saveMeasured, cfg
import numpy as np
import threading
import time
from hamamatsu.hamamatsu import xdHamamatsu
import png, os
from xdMeasurement import xdMeasurement

class ScannerHamamatsu(object):

    class LiveThread(threading.Thread):
        def __init__(self,scanner):
            threading.Thread.__init__(self)
            self._stop_event = threading.Event()
            self.scanner=scanner

        def stop(self):
            self._stop_event.set()

        def stopped(self):
            return self._stop_event.is_set()

        def run(self):
            if not self._stop_event.is_set():
                self.scanner._lin.go_move(self.scanner._start)
                self.scanner._lin.wait_for_stop()

            completeImage=""
            for position in np.arange(self.scanner._start, self.scanner._stop+0.308, 0.308): #0.308=22 px * 0.014 mm
                if not self._stop_event.is_set():

                    cmg.measureAll()

                    self.scanner._lin.go_move(position)

                    name=self.scanner._lin.name+f"_{float(self.scanner._lin.position):08.4f}"
                    saveMeasured(self.scanner._folderName, name)

                    if completeImage=="":
                        completeImage=xdMeasurement({"data":np.array(cmg.DS["Hamamatsu"].data["data"]).transpose()},self.scanner.save)
                    else:
                        completeImage.data["data"]=np.append(completeImage.data["data"], np.array(cmg.DS["Hamamatsu"].data["data"]).transpose())

                    self.scanner._lin.wait_for_stop()

            completeImage.data["data"]=np.reshape((completeImage.data["data"]), (-1, 2068)).clip(min=0)
            completeImage.save(os.path.join(cfg["Project"]["projectFolder"], self.scanner._folderName),self.scanner._folderName)



    def __init__(self,stage, start, stop, folderName):
        self._lin = stg.getHandler(stage)
        self._start=start
        self._stop=stop
        self._folderName=folderName
        self.start()

    def save(self, data:xdMeasurement, path):
        name=path.split("\\")[-1]
       # np.savetxt(os.path.join(path, f'{name}.csv'), data.data["data"].astype(int), fmt='%i', delimiter=";")
        mx = data.data["data"].max()
        png.from_array((data.data["data"] * int(2 ** 16 / mx)).astype(np.uint16), "L").save(os.path.join(path, f'{name}_{mx}.png'))

    def start(self):
        self._th= self.LiveThread(self)
        self._th.start()

    def stop(self):
        self._th.stop()

if __name__ == '__main__':
    from time import sleep

    lin = stg.open("HamamatsuStage")
    lin2 = stg.open("")
    cmg.openCamera(xdHamamatsu("Hamamatsu"))
    sleep(15)
    cmg.cameras["Hamamatsu"].setDarkImage(average=10)



    for a in range(0,5,2):
        lin2.go_move(a)
        lin.go_move(145)
        lin2.wait_for_stop()
        scan=ScannerHamamatsu("HamamatsuStage",145,150,f"test_{a}")
        while scan._th.is_alive():
            sleep(0.25)

    sleep(300)

    for a in range(0,250,2):
        lin2.go_move(a)
        lin.go_move(0)
        lin2.wait_for_stop()
        scan=ScannerHamamatsu("HamamatsuStage",0,150,f"pozice_{a}")
        while scan._th.is_alive():
            sleep(0.25)



    stg.close("HamamatsuStage")
    cmg.closeAll()
