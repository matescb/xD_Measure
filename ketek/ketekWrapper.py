from ctypes import *
from time import sleep
from os import path

from enum import IntEnum
import inspect
import numpy as np
import os

import platform

class Ketek():
    class LoggingLevel(IntEnum):
        MD_ERROR = 1
        MD_WARNING = 2
        MD_INFO = 3
        MD_DEBUG = 4

    class IOPriority(IntEnum):
        MD_IO_PRI_NORMAL = 0
        MD_IO_PRI_HIGH = 1


    def __init__(self, *args, **kwargs) -> None:
        super(Ketek, self).__init__(*args, **kwargs)

        path_lib = path.join(path.dirname(path.abspath(__file__)), "lib")
        os.environ['PATH'] =path_lib + os.pathsep + os.environ['PATH']

        if platform.system() == "Linux":
            self.lib = cdll.LoadLibrary(path.join(path_lib, 'libhandel.so'))

        elif platform.system() == "Windows":
            self.lib = cdll.LoadLibrary(path.join(path_lib, 'handel.dll'))

        print(path_lib)
        print(os.environ['PATH'])

    # status = This function print Error number. See handel_errors.h for more error details
    def checkErr(self, returnStatus):
        if returnStatus != 0:
            print("Error number: ", returnStatus)

    def xiaInit(self, initFilePath):
        initFile = create_string_buffer(initFilePath.encode('ascii'))
        status = self.lib.xiaInit(initFile)
        self.checkErr(status)

    def xiaInitHandel(self):
        # status = self.lib.xiaInitHandel(void)
        # self.checkErr(status)
        pass

    def xiaNewDetector(self):
        # status = self.lib.xiaNewDetector(char *alias)
        # self.checkErr(status)
        pass

    def xiaAddDetectorItem(self):
        # status = self.lib.xiaAddDetectorItem(char *alias, char *name, void *value)
        # self.checkErr(status)
        pass

    def xiaModifyDetectorItem(self):
        # status = self.lib.xiaModifyDetectorItem(char *alias, char *name, void *value)
        # self.checkErr(status)
        pass

    def xiaGetDetectorItem(self):
        # status = self.lib.xiaGetDetectorItem(char *alias, char *name, void *value)
        # self.checkErr(status)
        pass

    def xiaGetNumDetectors(self):
        # status = self.lib.xiaGetNumDetectors(unsigned int *numDet)
        # self.checkErr(status)
        pass

    def xiaGetDetectors(self):
        # status = self.lib.xiaGetDetectors(char *detectors[])
        # self.checkErr(status)
        pass

    def xiaGetDetectors_VB(self):
        # status = self.lib.xiaGetDetectors_VB(unsigned int index, char *alias)
        # self.checkErr(status)
        pass

    def xiaRemoveDetector(self):
        # status = self.lib.xiaRemoveDetector(char *alias)
        # self.checkErr(status)
        pass

    def xiaDetectorFromDetChan(self):
        # status = self.lib.xiaDetectorFromDetChan(int detChan, char *alias)
        # self.checkErr(status)
        pass

    def xiaNewFirmware(self):
        # status = self.lib.xiaNewFirmware(char *alias)
        # self.checkErr(status)
        pass

    def xiaAddFirmwareItem(self):
        # status = self.lib.xiaAddFirmwareItem(char *alias, char *name, void *value)
        # self.checkErr(status)
        pass

    def xiaModifyFirmwareItem(self):
        # status = self.lib.xiaModifyFirmwareItem(char *alias, unsigned short decimation,char *name, void *value)
        # self.checkErr(status)
        pass

    def xiaGetFirmwareItem(self):
        # status = self.lib.xiaGetFirmwareItem(char *alias, unsigned short decimation,char *name, void *value)
        # self.checkErr(status)
        pass

    def xiaGetNumFirmwareSets(self):
        # status = self.lib.xiaGetNumFirmwareSets(unsigned int *numFirmware)
        # self.checkErr(status)
        pass

    def xiaGetFirmwareSets(self):
        # status = self.lib.xiaGetFirmwareSets(char *firmware[])
        # self.checkErr(status)
        pass

    def xiaGetFirmwareSets_VB(self):
        # status = self.lib.xiaGetFirmwareSets_VB(unsigned int index, char *alias)
        # self.checkErr(status)
        pass

    def xiaGetNumPTRRs(self):
        # status = self.lib.xiaGetNumPTRRs(char *alias, unsigned int *numPTRR)
        # self.checkErr(status)
        pass

    def xiaRemoveFirmware(self):
        # status = self.lib.xiaRemoveFirmware(char *alias)
        # self.checkErr(status)
        pass

    def xiaNewModule(self):
        # status = self.lib.xiaNewModule(char *alias)
        # self.checkErr(status)
        pass

    def xiaAddModuleItem(self):
        # status = self.lib.xiaAddModuleItem(char *alias, char *name, void *value)
        # self.checkErr(status)
        pass

    def xiaModifyModuleItem(self):
        # status = self.lib.xiaModifyModuleItem(char *alias, char *name, void *value)
        # self.checkErr(status)
        pass

    def xiaGetModuleItem(self):
        # status = self.lib.xiaGetModuleItem(char *alias, char *name, void *value)
        # self.checkErr(status)
        pass

    def xiaGetNumModules(self):
        # status = self.lib.xiaGetNumModules(unsigned int *numModules)
        # self.checkErr(status)
        pass

    def xiaGetModules(self):
        # status = self.lib.xiaGetModules(char *modules[])
        # self.checkErr(status)
        pass

    def xiaGetModules_VB(self):
        # status = self.lib.xiaGetModules_VB(unsigned int index, char *alias)
        # self.checkErr(status)
        pass

    def xiaRemoveModule(self):
        # status = self.lib.xiaRemoveModule(char *alias)
        # self.checkErr(status)
        pass

    def xiaModuleFromDetChan(self):
        # status = self.lib.xiaModuleFromDetChan(int detChan, char *alias)
        # self.checkErr(status)
        pass

    def xiaAddChannelSetElem(self):
        # status = self.lib.xiaAddChannelSetElem(unsigned int detChanSet, unsigned int newChan)
        # self.checkErr(status)
        pass

    def xiaRemoveChannelSetElem(self):
        # status = self.lib.xiaRemoveChannelSetElem(unsigned int detChan, unsigned int chan)
        # self.checkErr(status)
        pass

    def xiaRemoveChannelSet(self):
        # status = self.lib.xiaRemoveChannelSet(unsigned int detChan)
        # self.checkErr(status)
        pass

    def xiaStartSystem(self):
        status = self.lib.xiaStartSystem()
        self.checkErr(status)

    def xiaDownloadFirmware(self):
        # status = self.lib.xiaDownloadFirmware(int detChan, char *type)
        # self.checkErr(status)
        pass

    def xiaSetAcquisitionValues(self,detChan,name,value):
        name = create_string_buffer(str.encode(name))
        status = self.lib.xiaSetAcquisitionValues(detChan, name, c_double(value))
        self.checkErr(status)

    def xiaGetAcquisitionValues(self):
        # status = self.lib.xiaGetAcquisitionValues(int detChan, char *name, void *value)
        # self.checkErr(status)
        pass

    def xiaRemoveAcquisitionValues(self):
        # status = self.lib.xiaRemoveAcquisitionValues(int detChan, char *name)
        # self.checkErr(status)
        pass

    def xiaUpdateUserParams(self):
        # status = self.lib.xiaUpdateUserParams(int detChan)
        # self.checkErr(status)
        pass

    def xiaGainOperation(self,detChan, valueIn):
        name = create_string_buffer(b"calibrate_gain_trim")
        status = self.lib.xiaGainOperation(detChan, name, c_double(valueIn))
        self.checkErr(status)

    def xiaGainCalibrate(self):
        # status = self.lib.xiaGainCalibrate(int detChan, double deltaGain)
        # self.checkErr(status)
        pass

    class Resume(IntEnum):
        resume=1
        clear=0

    def xiaStartRun(self, detChan, resume:Resume):
        status = self.lib.xiaStartRun(c_int(detChan), c_short(resume))
        self.checkErr(status)

    def xiaStopRun(self, detChan):
        status = self.lib.xiaStopRun(c_int(detChan))
        self.checkErr(status)

    def xiaGetRunDataLength(self, detChan):
        name = create_string_buffer(b"mca_length")
        value = c_long(0)
        status = self.lib.xiaGetRunData(c_int(detChan), name, byref(value))
        self.checkErr(status)
        return value.value

    def xiaGetRunData(self, detChan):
        name = create_string_buffer(b"mca")
        value = (c_long * self.xiaGetRunDataLength(detChan))(0)
        status = self.lib.xiaGetRunData(c_int(detChan), name, byref(value))
        self.checkErr(status)
        return value[:]

    def xiaDoSpecialRun(self):
        # status = self.lib.xiaDoSpecialRun(int detChan, char *name, void *info)
        # self.checkErr(status)
        pass

    def xiaGetSpecialRunData(self):
        # status = self.lib.xiaGetSpecialRunData(int detChan, char *name, void *value)
        # self.checkErr(status)
        pass

    def xiaLoadSystem(self):
        # status = self.lib.xiaLoadSystem(char *type, char *filename)
        # self.checkErr(status)
        pass

    def xiaSaveSystem(self):
        # status = self.lib.xiaSaveSystem(char *type, char *filename)
        # self.checkErr(status)
        pass

    def xiaGetParameter(self):
        # status = self.lib.xiaGetParameter(int detChan, const char *name, unsigned short *value)
        # self.checkErr(status)
        pass

    def xiaSetParameter(self):
        # status = self.lib.xiaSetParameter(int detChan, const char *name, unsigned short value)
        # self.checkErr(status)
        pass

    def xiaGetNumParams(self):
        # status = self.lib.xiaGetNumParams(int detChan, unsigned short *numParams)
        # self.checkErr(status)
        pass

    def xiaGetParamData(self):
        # status = self.lib.xiaGetParamData(int detChan, char *name, void *value)
        # self.checkErr(status)
        pass

    def xiaGetParamName(self):
        # status = self.lib.xiaGetParamName(int detChan, unsigned short index, char *name)
        # self.checkErr(status)
        pass

    class microDXPConstants(IntEnum):
        AV_MEM_ALL = 0x0
        AV_MEM_NONE = 0x01
        AV_MEM_REQ = 0x02
        AV_MEM_PARSET = 0x04
        AV_MEM_GENSET = 0x08
        AV_MEM_FIPPI = 0x10
        AV_MEM_ADC = 0x20
        AV_MEM_GLOB = 0x40
        AV_MEM_CUST = 0x80


    def xiaBoardOperation(self,detChan,name,value):
        name = create_string_buffer(str.encode(name))

        status = self.lib.xiaBoardOperation(detChan, name, c_double(value))
        self.checkErr(status)
        pass

    def xiaMemoryOperation(self):
        # status = self.lib.xiaMemoryOperation(int detChan, char *name, void *value)
        # self.checkErr(status)
        pass

    def xiaCommandOperation(self):
        # status = self.lib.xiaCommandOperation(int detChan, byte_t cmd,unsigned int lenS, byte_t *send,unsigned int lenR, byte_t *recv)
        # self.checkErr(status)
        pass

    def xiaExit(self):
        status = self.lib.xiaExit()
        self.checkErr(status)
        pass

    def xiaEnableLogOutput(self):
        status = self.lib.xiaEnableLogOutput()
        self.checkErr(status)


    def xiaSuppressLogOutput(self):
        # status = self.lib.xiaSuppressLogOutput(void)
        # self.checkErr(status)
        pass

    def xiaSetLogLevel(self, level: LoggingLevel):
        status = self.lib.xiaSetLogLevel(c_int(level))
        self.checkErr(status)

    def xiaSetLogOutput(self, logFilePath):
        logFile = create_string_buffer(logFilePath.encode('ascii'))
        status = self.lib.xiaSetLogOutput(logFile)
        self.checkErr(status)

    def xiaCloseLog(self):
        status = self.lib.xiaCloseLog()
        self.checkErr(status)
        pass

    def xiaSetIOPriority(self, priority:IOPriority):
        status = self.lib.xiaSetIOPriority(c_int(priority))
        self.checkErr(status)
        pass

    # status =HANDEL_IMPORT void     HANDEL_API xiaGetVersionInfo(int * rel, int * min, int * maj, char * pretty):

    # status =HANDEL_IMPORT char * HANDEL_API xiaGetErrorText(int errorcode):
