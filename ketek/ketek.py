
from xdSpectral.xdSpectralClass import xdSpectral
from xdCore import spm
import os, time
import numpy as np

import matplotlib.pyplot as plt


if __name__ == "__main__":
    from ketekWrapper import Ketek
else:
    from .ketekWrapper import Ketek

from xdMeasurement import xdMeasurement
from xdCore import cfg

class xdKetek(Ketek, xdSpectral):
    def __init__(self, name,inifile, detchan) -> None:
        super(xdKetek,self).__init__(name)
        self.inifile=inifile
        self.xiaSetLogLevel(self.LoggingLevel.MD_INFO)
        self.xiaSetLogOutput(os.path.join(cfg["Project"]["projectFolder"], f"Ketek_{name}_chan{detchan}.log"))
        self.xiaSetIOPriority(self.IOPriority.MD_IO_PRI_HIGH)
        
        self._detchan=detchan

    def __del__(self):
        self.close()

    def open(self):
       # super().open()
        self.xiaEnableLogOutput()
        self.xiaInit(self.inifile)
        self.xiaStartSystem()
        self.__fineGain=0.978
        #self.fineGain=self.__fineGain

        '''
        binWidth = 1
        nMCA = 2048
        threshold = 30.0
        gain = 1.0
        polarity = 1.0
        resetInt = 100.0

        parsetAndGenset=self.microDXPConstants.AV_MEM_PARSET | self.microDXPConstants.AV_MEM_GENSET

        self.xiaSetAcquisitionValues(self._detchan, "mca_bin_width", binWidth)
        self.xiaSetAcquisitionValues(self._detchan, "number_mca_channels",nMCA)
        self.xiaSetAcquisitionValues(self._detchan, "trigger_threshold", threshold)
        self.xiaSetAcquisitionValues(self._detchan, "gain", gain)
        self.xiaSetAcquisitionValues(self._detchan, "polarity", polarity)
        self.xiaSetAcquisitionValues(self._detchan, "preamp_value", resetInt)
        self.xiaBoardOperation(self._detchan, "apply", parsetAndGenset)

        genset = self._detchan
        parset = self._detchan
        self.xiaBoardOperation(self._detchan, "save_genset", genset)
        self.xiaBoardOperation(self._detchan, "save_parset", parset)
        '''


    def close(self):
        super().close()
        self.xiaCloseLog()
        self.xiaExit()

    @property
    def fineGain(self):
        return self.__fineGain

    @fineGain.setter
    def fineGain(self,value):
        self.xiaGainOperation(self._detchan, value)
        self.__fineGain=value

    # This function start new MCA spectrum capturing. Latest results can be read out any time by getRun function.
    def start(self):
        self.lib.xiaStartRun(self._detchan, self.Resume.clear)

    def resume(self):
        self.lib.xiaStartRun(self._detchan, self.Resume.resume)

    # This funcstion stop MCA spectrum capturing
    def stop(self):
        self.lib.xiaStopRun(self._detchan)

    # This function return MCA spectrum
    def read(self):
        # Prepare to read out MCA spectrum
        if self.xiaGetRunDataLength(self._detchan):
            spm.addMeasured(xdMeasurement({"data":self.xiaGetRunData(self._detchan), "binSize":self.binSize, "detChan":self._detchan},self.save), self.name)
            self.resume()
            return True
        else:
            return False

    def save(self, data:xdMeasurement, path):
        with open(os.path.join(path,f"Ketek_{self.name}.txt"), 'w', encoding='utf-8') as f:
            f.write(str(data.data))


        plt.plot(np.arange(0, len(data.data["data"]) * data.data["binSize"], data.data["binSize"]), data.data["data"])
        plt.ylabel('Counts')
        plt.xlabel('keV')
        plt.savefig(os.path.join(path,f"Ketek_{self.name}.png"), dpi=500)
        plt.clf()

if __name__ == "__main__":
    ket=xdKetek("Ketek","KETEK_DPP2_usb2.ini",0)
    ket.open()
    time.sleep(10)