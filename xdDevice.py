
class xdDevice():
    name="Unknown_xdDevice"
    def __init__(self, name:str, *args, **kwargs) -> None:
        super(xdDevice, self).__init__(*args, **kwargs)

        self.name = name
    
    def __del__(self):
        super(xdDevice, self).__del__()

    def open(self):
        pass

    def close(self):
        pass