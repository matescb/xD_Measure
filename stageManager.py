from standa.standa import Standa
import serial.tools.list_ports
import sys
import glob
import serial
from standa.standa import get_probe
from time import sleep


class StageManager():
    stagesAll = {}
    stagesOpened = {}

    def __init__(self):
        self.scanForStages()
        pass

    def __del__(self):
        for s in list(self.stagesOpened):
            del self.stagesOpened[s]

    @property
    def positions(self):
        l = []
        for s in list(self.stagesOpened):
            l.append(dict(stage=s, position=self.stagesOpened[s].position))
        return l

    def tryName(self, stageName):
        return stageName in self.stagesAll

    def open(self, stageName):
        # TODO: Ochrana vstupu
        self.stagesOpened[stageName] = Standa(self.stagesAll[stageName])
        del self.stagesAll[stageName]
        return self.stagesOpened[stageName]

    def getHandler(self, name):
        if name in list(self.stagesOpened):
            return self.stagesOpened[name]

    def go_stopAll(self):
        for s in self.stagesOpened:
            self.stagesOpened[s].go_stop()

    def go_emergencyStopAll(self):
        for s in self.stagesOpened:
            self.stagesOpened[s].go_emergencyStop()

    def go_zeroAll(self):
        for s in self.stagesOpened:
            self.stagesOpened[s].go_move(0)

    def go_homeAll(self):
        for s in self.stagesOpened:
            self.stagesOpened[s].go_home()

    def close(self, stageName):
        # TODO: Ochrana vstupu
        self.stagesAll[stageName] = self.stagesOpened[stageName].port
        self.stagesOpened[stageName].close()
        del self.stagesOpened[stageName]

    def scanForStages(self):
        """ Lists serial port names

            :raises EnvironmentError:
                On unsupported or unknown platforms
            :returns:
                A list of the serial ports available on the system
        """
        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            # this excludes your current terminal "/dev/tty"
            ports = glob.glob('/dev/tty[A-Za-z]*')
        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')
        else:
            raise EnvironmentError('Unsupported platform')

        self.stagesAll = {}
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                result, name = get_probe(port)
                if result:
                    self.stagesAll[str(name)] = port
            except (OSError, serial.SerialException):
                pass
        return len(self.stagesAll)


if __name__ == "__main__":
    lin = StageManager()
    lin.scanForStages()

    lin.open("HamamatsuStage")
    lin.open("pricne")
    lin.open("podelne")

    lin.stagesOpened["HamamatsuStage"].go_left()
    lin.stagesOpened["pricne"].go_left()
    lin.stagesOpened["podelne"].go_left()
    sleep(0.5)
    lin.go_stopAll()
    sleep(0.5)

    del lin
    sys.exit()
