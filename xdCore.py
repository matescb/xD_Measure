from stageManager import StageManager
import configparser
from xdCam.xdCameraManager import CameraManager
from xdSpectral.xdSpectralManager import SpectralManager
# datetime object containing current date and time
import numpy as np

import datetime,os
import logging


print(datetime.datetime.now())
cfg = configparser.ConfigParser()
cfg.read("d:\DP\QT_playing\config.ini")
now=datetime.datetime.now()
logging.basicConfig(format='%(asctime)s %(levelname)-8s %(message)s',
                    filename=os.path.join(cfg["Project"]["projectFolder"],f"XDmeasure_{now.strftime('%Y%m%d_%H%M_%S,%f')[:-4]}.log"),
                    datefmt='%Y-%m-%d %H:%M:%S', level=logging.INFO)

logging.info('XDmeasure started')
logging.info('Project folder is {cfg["Project"]["projectFolder"]}')

def cfgSave():
    with open("config.ini","w") as f:
        cfg.write(f)
    logging.info('Config file saved')

stg = StageManager()
cmg = CameraManager()
spm = SpectralManager()

def saveMeasured(scanFolder=False, scanName="time"):

    logging.info(f'Save measurenment as {scanName}')

    projectFolder = cfg["Project"]["projectFolder"]

    if not os.path.exists(projectFolder):
        os.mkdir(projectFolder)

    if scanFolder:
        projectFolder=os.path.join(projectFolder, scanFolder)

        if not os.path.exists(projectFolder):
            os.mkdir(projectFolder)

    path=""
    if scanName!="time":
        path=os.path.join(projectFolder, scanName)

        if os.path.exists(path):
            iterator=2
            while os.path.exists(os.path.join(projectFolder, scanName+f"_{iterator}")):
                iterator=iterator+1
            path=os.path.join(projectFolder, scanName+f"_{iterator}")
    else:
        now=datetime.datetime.now()
        folder=now.strftime("%Y%m%d_%H%M_%S")
        path=os.path.join(projectFolder,folder)

        if os.path.exists(path):
            folder = now.strftime("%Y%m%d_%H%M_%S,%f")[:-4]
            path = os.path.join(projectFolder, folder)
    os.mkdir(path)

    spm.saveToFolder(path)
    cmg.saveToFolder(path)


