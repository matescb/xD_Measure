import sys, os

sys.path.append(os.path.dirname(os.path.abspath(__file__))+r"/PIXetPro/") #add the pixet folder to the filepath to import the pypixet module

if __name__ == "__main__":
    import PIXetPro.pypixet as pypixet
else:
    import pypixet as pypixet

from xdCam.xdCamera import xdCamera
from xdCore import cmg
from ctypes import *
import numpy as np
import png
from xdMeasurement import xdMeasurement
import datetime
import secrets

class xdTimepix(xdCamera):

    def __init__(self, name: str) -> None:
        super(xdTimepix, self).__init__(name)

        pypixet.start()  # initialize pixet
        self.pixet = pypixet.pixet  # get pixet A

        # first list all Medipix/Timepix devices and use the first one:
        devices = self.pixet.devicesByType(self.pixet.PX_DEVTYPE_MPX2)
        if not devices:
            raise Exception("No devices connected")
        self.device = devices[0]  # use  the first device
        self.device.loadConfigFromFile(r"d:\DP\QT_playing\timepix\config\MiniPIX-E04-W0332.xml")

        self.acqTime=1
        self.acqCount=1

    def __del__(self):
        pass

    def open(self):
        pass

    def close(self):
        self.pixet.exitPixet()

    @property
    def exposure(self):
        return self.acqTime

    @exposure.setter
    def exposure(self, exp):
        self.acqTime=exp
        pass

    @property
    def gain(self):
        return self.acqCount

    @gain.setter
    def gain(self, exp):
        self.acqCount=exp

    @property
    def sensorSize(self):

        return (256,256)
    def setDarkImage(self, average):
        self.dark = self.getMeasure(average=average, dark=False)

    def getMeasure(self, average=1, dark=True):

        if not os.path.exists("tmp"):
            os.mkdir("tmp")

        now=datetime.datetime.now()
        folder=r"tmp/" + self.name+ "_" + now.strftime("%Y%m%d_%H%M_%S,%f")[:-4]+ "_" + secrets.token_hex(nbytes=8)

        os.mkdir(folder)
        outputFile=folder + r"/" + self.name+ "_" + now.strftime("%Y%m%d_%H%M_%S,%f")[:-4]+".txt"

        #rc = self.device.doSimpleIntegralAcquisition(acqCount, acqTime, self.pixet.PX_FTYPE_AUTODETECT, outputFile)
        rc = self.device.doAdvancedAcquisition(self.acqCount, self.acqTime, self.pixet.PX_ACQTYPE_FRAMES, self.pixet.PX_ACQMODE_NORMAL, self.pixet.PX_FTYPE_AUTODETECT,0, outputFile)

        print("Acquition: %d" % rc)
        frame=self.device.lastAcqFrameRefInc()
        data_arr = np.array(frame.data())

        if __name__ == "__main__":
            return np.reshape(data_arr, (256, 256))
        else:
            cmg.addMeasured(xdMeasurement({"data":np.reshape(data_arr, (256, 256)), "link":folder}, self.save), self.name)

    def save(self, data:xdMeasurement, path):
        np.savetxt(os.path.join(path, "ZlatyTimepix.csv"), data.data["data"].astype(int), fmt='%i', delimiter=";")


        mx=data.data["data"].max()
        if mx==0:
            mx=1

        png.from_array((data.data["data"]*int(2**16/mx)).astype(np.uint16), "L").save(os.path.join(path, "ZlatyTimepix.png"))


        import distutils.dir_util
        from_dir = data.data["link"]
        to_dir = path
        try:
            distutils.dir_util.copy_tree(from_dir, to_dir)
        except:
            print("Issue during copy")

if __name__ == "__main__":
    tpx=xdTimepix("Timepix")
    tpx.open()
    print(tpx.getMeasure())
    print("END")