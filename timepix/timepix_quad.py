import sys, os

sys.path.append(os.path.dirname(
    os.path.abspath(__file__)) + r"/PIXetPro/")  # add the pixet folder to the filepath to import the pypixet module

if __name__ == "__main__":
    import PIXetPro.pypixet as pypixet
else:
    import pypixet as pypixet

from xdCam.xdCamera import xdCamera
from xdCore import cmg
from ctypes import *
import numpy as np
import png
from xdMeasurement import xdMeasurement
import datetime
import secrets
import threading, logging

class xdTimepixQuad(xdCamera):

    def __init__(self, name: str, chips) -> None:
        super(xdTimepixQuad, self).__init__(name)

        pypixet.start()  # initialize pixet
        self.pixet = pypixet.pixet  # get pixet A

        conf = {"K05": r"d:\DP\QT_playing\timepix\config\quad1\5keV\K05-W0051.xml",
                "J10": r"d:\DP\QT_playing\timepix\config\quad1\5keV\J10-W0051.xml",
                "I06": r"d:\DP\QT_playing\timepix\config\quad1\5keV\I06-W0051.xml",
                "C10": r"d:\DP\QT_playing\timepix\config\quad1\5keV\C10-W0051.xml",

                "C09": r"d:\DP\QT_playing\timepix\config\quad2\5keV\C09-W0058.xml",
                "G11": r"d:\DP\QT_playing\timepix\config\quad2\5keV\G11-W0058.xml",
            "J08": r"d:\DP\QT_playing\timepix\config\quad2\5keV\J08-W0058.xml",
                "G10": r"d:\DP\QT_playing\timepix\config\quad2\5keV\G10-W0058.xml",

                "00": r"d:\DP\QT_playing\timepix\config\quad2\5keV\G10-W0058.xml",

                }


        self.chips = chips

        # first list all Medipix/Timepix devices and use the first one:
        devices = self.pixet.devicesByType(self.pixet.PX_DEVTYPE_TPX3)

        self.quad = {}
        for a in range(0, len(devices)):
            if devices[a].deviceID()[:3] in self.chips.values():
                self.quad[devices[a].deviceID()[:3]] = devices[a]
        #del (self.quad["00"])

        for a in list(self.quad):
            self.quad[a].loadConfigFromFile(conf[a])

        self.acqTime = 1
        self.makefilter()
        self.acqCount = 1

    def __del__(self):
        pass

    def makefilter(self):
        f = lambda x: 1 if x != 0 else 0
        self.filter = np.vectorize(f)((np.loadtxt(r"d:\DP\QT_playing\timepix\config\Quad1\filter.csv", delimiter=';')))

    def open(self):
        pass

    def close(self):
        self.pixet.exitPixet()

    @property
    def exposure(self):
        return self.acqTime

    @exposure.setter
    def exposure(self, exp):
        self.acqTime = exp
        pass

    @property
    def gain(self):
        return self.acqCount

    @gain.setter
    def gain(self, exp):
        self.acqCount = exp

    @property
    def sensorSize(self):

        return (256, 256)

    def setDarkImage(self, average):
        self.dark = self.getMeasure(average=average, dark=False)

    def acqThread(self,dev, id, outputFile):
        print("aaaa")
        logging.info(id + ': Measuring ... ')

        dev.doSimpleIntegralAcquisition(self.acqCount, self.acqTime, self.pixet.PX_FTYPE_AUTODETECT, outputFile)
        self.data[id] = (np.resize(self.quad[id].lastAcqDataRefInc().subFrames()[1].data(), (256, 256)))

        logging.info(id + ': Finished.')

    def getMeasure(self, average=1, filtered=False, ret=0):
        from time import sleep
        if not os.path.exists("tmp"):
            os.mkdir("tmp")

        now=datetime.datetime.now()
        folder=r"tmp/" + self.name+ "_" + now.strftime("%Y%m%d_%H%M_%S,%f")[:-4]+ "_" + secrets.token_hex(nbytes=8)

        os.mkdir(folder)

        self.data = {}

        threads = []
        for a in list(self.quad):
            outputFile = folder + r"/" + self.name + "_" + str(a) + "_" + now.strftime("%Y%m%d_%H%M_%S,%f")[:-4] + ".txt"
            threads.append(threading.Thread(target=self.acqThread, args=(self.quad[a], a, outputFile)))
            threads[-1].start()
            #        acqThread(device, id, data)

            # wait
        sleep(self.acqCount * self.acqTime)

        # join all threads
        for t in threads:
            t.join()

        data=self.data
        for a in list(self.quad):
            data[a] = (np.resize(self.quad[a].lastAcqDataRefInc().subFrames()[1].data(), (256, 256)))
        # convert to numpy array and reshape to 2D

        if self.chips[1] not in data:
            print('MISSING CHIP {}!!!'.format(self.chips[1]))
            data[self.chips[1]] = [0] * 65536
        #        break
        if self.chips[2] not in data:
            print('MISSING CHIP {}!!!'.format(self.chips[2]))
            data[self.chips[2]] = [0] * 65536
        #        break
        if self.chips[3] not in data:
            print('MISSING CHIP {}!!!'.format(self.chips[3]))
            data[self.chips[3]] = [0] * 65536
        #        break
        if self.chips[4] not in data:
            print('MISSING CHIP {}!!!'.format(self.chips[4]))
            data[self.chips[4]] = [0] * 65536



        data[self.chips[1]] = np.resize(data[self.chips[1]], (256, 256))
        data[self.chips[2]] = np.resize(data[self.chips[2]], (256, 256))
        data[self.chips[3]] = np.resize(data[self.chips[3]], (256, 256))
        data[self.chips[4]] = np.resize(data[self.chips[4]], (256, 256))


        # add edge pixels
        tilesize = 258
        data1 = np.zeros((tilesize, tilesize))
        data2 = np.zeros((tilesize, tilesize))
        data3 = np.zeros((tilesize, tilesize))
        data4 = np.zeros((tilesize, tilesize))
        data1[1:tilesize - 1, 1:tilesize - 1] = data[self.chips[1]]
        data2[1:tilesize - 1, 1:tilesize - 1] = data[self.chips[2]]
        data3[1:tilesize - 1, 1:tilesize - 1] = data[self.chips[3]]
        data4[1:tilesize - 1, 1:tilesize - 1] = data[self.chips[4]]

        # split edge signal
        data1[0:2, 1:-1] = data[self.chips[1]][0, :] / 2
        data2[0:2, 1:-1] = data[self.chips[2]][0, :] / 2
        # data3[0:2, 1:-1] = data[self.chips[3]][0, :] / 2
        data4[0:2, 1:-1] = data[self.chips[4]][0, :] / 2
        data1[-2:, 1:-1] = data[self.chips[1]][-1, :] / 2
        data2[-2:, 1:-1] = data[self.chips[2]][-1, :] / 2
        data3[-2:, 1:-1] = data[self.chips[3]][-1, :] / 2
        data4[-2:, 1:-1] = data[self.chips[4]][-1, :] / 2
        data1[1:-1, 0:2] = np.transpose(data[self.chips[1]][:, 0][None]) / 2
        data2[1:-1, 0:2] = np.transpose(data[self.chips[2]][:, 0][None]) / 2
        data3[1:-1, 0:2] = np.transpose(data[self.chips[3]][:, 0][None]) / 2
        data4[1:-1, 0:2] = np.transpose(data[self.chips[4]][:, 0][None]) / 2
        # data1[1:-1, -2: ] = np.transpose(data[self.chips[1]][:, -1][None]) / 2
        # data2[1:-1, -2: ] = np.transpose(data[self.chips[2]][:, -1][None]) / 2
        data3[1:-1, -2:] = np.transpose(data[self.chips[3]][:, -1][None]) / 2
        # data4[1:-1, -2: ] = np.transpose(data[self.chips[4]][:, -1][None]) / 2
        data1[0, 0] = data[self.chips[1]][0, 0] / 4
        data2[0, 0] = data[self.chips[1]][0, 0] / 4
        # data3[0, 0] = data[self.chips[1]][0, 0] / 4
        data4[0, 0] = data[self.chips[1]][0, 0] / 4
        # data1[0, -1] = data[self.chips[1]][0, -1] / 4
        # data2[0, -1] = data[self.chips[2]][0, -1] / 4
        # data3[0, -1] = data[self.chips[3]][0, -1] / 4
        # data4[0, -1] = data[self.chips[4]][0, -1] / 4

        data1[-1, 0] = data[self.chips[1]][-1, 0] / 4
        data2[-1, 0] = data[self.chips[2]][-1, 0] / 4
        data3[-1, 0] = data[self.chips[3]][-1, 0] / 4
        data4[-1, 0] = data[self.chips[4]][-1, 0] / 4
        # data1[-1, -1] = data[self.chips[1]][-1, -1] / 4
        # data2[-1, -1] = data[self.chips[2]][-1, -1] / 4
        data3[-1, -1] = data[self.chips[3]][-1, -1] / 4
        # data4[-1, -1] = data[self.chips[4]][-1, -1] / 4

        # rotate
        data1 = np.rot90(data1)
        data3 = np.rot90(data3, k=2)
        data4 = np.rot90(data4, k=2)

        # merge data  (orientation = connectors to the right)
        offset = 35
        merged = np.zeros((2 * tilesize - offset, 2 * tilesize - offset))
        merged[tilesize - offset:2 * tilesize - offset, 0:tilesize - offset] = data4[:, offset:tilesize]
        merged[tilesize:2 * tilesize - offset, tilesize - offset:2 * tilesize - offset] = data3[0:tilesize - offset, :]
        merged[0:tilesize - offset, 0:tilesize] = data1[offset:tilesize, :]
        merged[0:tilesize, tilesize:2 * tilesize - offset] = data2[:, 0:tilesize - offset]

        # cut edges
        edge = 3
        merged = merged[edge:-1 - edge, edge:-1 - edge]

        # rotate (orientation = connectors down)
        merged = np.rot90(merged, k=3)

        if filtered:
            merged=np.ma.masked_array(data=merged, mask=self.filter, fill_value=np.nan).filled(0)

            f = lambda x: 1 if x > 50 else 0
            self.filter2 = np.vectorize(f)((np.loadtxt(r"d:\DP\QT_playing\timepix\config\Quad1\filter.csv", delimiter=';')))
            merged=np.ma.masked_array(data=merged, mask=self.filter2, fill_value=np.nan).filled(0)

        if __name__ == "__main__" or ret>0:
            return merged
        else:
            cmg.addMeasured(xdMeasurement({"data":merged, "link":folder},self.save), self.name)

    def save(self, data:xdMeasurement, path):
        np.savetxt(os.path.join(path, f"{self.name}.csv"), data.data["data"].astype(int), fmt='%i', delimiter=";")

        mx = data.data["data"].max()
        png.from_array((data.data["data"] * int(2 ** 16 / mx)).astype(np.uint16), "L").save(os.path.join(path, f"{self.name}.png"))
        import distutils.dir_util
        from_dir = data.data["link"]
        to_dir = path

        try:
            distutils.dir_util.copy_tree(from_dir, to_dir)
        except:
            print("Issue during copy")

if __name__ == "__main__":
    tpx = xdTimepixQuad("Timepix", {0: '00', 1: 'C09', 2: 'G11', 3: 'J08', 4: 'G10'})
    tpx.open()
    a=(tpx.getMeasure())
    print("END")
