from xdCam.xdCamera import xdCamera
from xdCore import cmg

if __name__ == "__main__":
    from hamamatsuWrapper import Hamamatsu
else:
    from .hamamatsuWrapper import Hamamatsu

from ctypes import *
import numpy as np
from xdMeasurement import xdMeasurement
import os
import png

class xdHamamatsu(xdCamera, Hamamatsu):

    def __init__(self, name: str) -> None:
        super(xdHamamatsu, self).__init__(name)
        self.dark=np.zeros(43428)

    def open(self):
        self.dcamOpen()
        self.dcamSetCCDType(self.CCDType.DCAM_CCD_TYPE2)

        self.lib.DcamGetBitPerPixel(byref(self.nBitSize))  # Bit per pixel

    def close(self):
        self.lib.DcamClose()


    @property
    def exposure(self):
        return self.dcamGetExposureTime()

    @exposure.setter
    def exposure(self, exp):
        self.dcamSetExposureTime(exp)

    @property
    def gain(self):
        return self.dcamGetGain()

    @gain.setter
    def gain(self, gain):
        self.dcamSetGain(gain)

    @property
    def sensorSize(self):
        pass

    def setDarkImage(self, average=5):
        self.dark = self.getMeasure(average=average, dark=False)

    def getMeasure(self, average=1, dark=True):
        n_width, n_height = self.dcamGetImageSize()
        p_data_buff = (c_int16 * (n_width * n_height))()
        stat = c_int()
        avg = np.empty(0)

        #self.setLed(1)
        self.lib.DcamStop()

        for b in range(0, average):
            self.lib.DcamCapture(p_data_buff, ((n_width) * (n_height) * sizeof(c_int16)))
            self.lib.DcamWait(byref(stat), 5000)  # TODO neosetreny timeout

            avg = np.append(avg, (np.array(p_data_buff[n_width * self.drop_beginning_lines:])))

        avg = np.mean(np.reshape(avg, (-1, n_width * (n_height - 1))), axis=0)
        #self.setLed(0)
        #self.setLed(0)

        if dark:
            avg = avg - self.dark
        d={}
        d["data"]=np.reshape(avg, (-1, n_width)).transpose()
        cmg.addMeasured(xdMeasurement(d, self.save), self.name)

        return avg

    def save(self, data:xdMeasurement, path):
        #np.savetxt(os.path.join(path, "Hamamatsu.csv"), data.data["data"].astype(int), fmt='%i', delimiter=";")
        #mx = data.data["data"].max()
        #png.from_array((data.data["data"] * int(2 ** 16 / mx)).astype(np.uint16), "L").save(os.path.join(path, "Hamamatsu.png"))
        pass