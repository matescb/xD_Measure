from ctypes import *
from time import sleep
from os import path

from enum import IntEnum
import inspect
import numpy as np


class Hamamatsu():
    class ErrorCode(IntEnum):
        failed_ToObtain = -1  #
        dcCode_Success = 0  # Normal termination
        dcCode_Unknown = 1  # An unknown error has occurred
        dcCode_NoInit = 2  # Library is not initialized
        dcCode_AlreadyInit = 3  # Already in-use
        dcCode_NoDriver = 4  # No driver was found
        dcCode_NoMemory = 5  # Memory is insufficient
        dcCode_NotConnected = 6  # The device is not connected
        dcCode_InvalidParam = 9  # Invalid parameter
        dcCode_DeviceDefect = 100  # The device is not functioning
        dcCode_Overrun = 110  # Overrun has occurred
        dcCode_Timeout = 111  # Timeout has occurred
        dcCode_AlreadyStart = 120  # Already started
        dcCode_CoolingOn = 200  # Already cooling control started.
        dcCode_CoolingOff = 201  # Cooling control stopped.
        dcCode_FailedCoolingCtrl = 202  # Failed comminucation to Cooling controler.

    class BinningType(IntEnum):
        """ Binning type

        Attributes:
            DCAM_BINNING_AREA: Area Scanning
            DCAM_BINNING_FULL: Full Line Binning
        """
        DCAM_BINNING_AREA = 0
        DCAM_BINNING_FULL = 1

    class BitPixel(IntEnum):
        """ The number of bits per pixel

        Attributes:
            DCAM_BITPIXEL_8: 8 Bit
            DCAM_BITPIXEL_10: 10 Bit
            DCAM_BITPIXEL_12: 12 Bit
            DCAM_BITPIXEL_14: 14 Bit
            DCAM_BITPIXEL_16: 16 Bit
        """
        DCAM_BITPIXEL_8 = 8  # 8 Bit
        DCAM_BITPIXEL_10 = 10  # 10 Bit
        DCAM_BITPIXEL_12 = 12  # 12 Bit
        DCAM_BITPIXEL_14 = 14  # 14 Bit
        DCAM_BITPIXEL_16 = 16  # 16 Bit

    class ImageAcquisition(IntEnum):
        # [Image acquisition]
        DCAM_WAITSTATUS_COMPLETED = 0  # Image acquisition is complete.
        DCAM_WAITSTATUS_UNCOMPLETED = 1  # Image acquisition is not complete.
        DCAM_WAIT_INFINITE = -1  # Wait until image acquisition is complete.

    class DeviceState(IntEnum):
        # [Device state]
        DCAM_DEVSTATE_NON = 0  # Non-connection, No device found
        DCAM_DEVSTATE_DEVICE = 1  # Non-connection, Device found
        DCAM_DEVSTATE_NODEVICE = 2  # Connection, No device found
        DCAM_DEVSTATE_CONNECT = 3  # Connection, Device found
        DCAM_DEVSTATE_BOOT = 4  # Connection, Device found(during the boot process)

    class TriggerMode(IntEnum):
        # [Trigger mode]
        # C10785
        # C11287
        # C11288 correspondence
        DCAM_TRIGMODE_INT = 0  # Internal Mode
        DCAM_TRIGMODE_EXT_EDGE = 1  # External Trigger Edge Mode
        DCAM_TRIGMODE_EXT_LEVEL = 2  # External Trigger Level Mode

    class TriggerPolarity(IntEnum):
        # [Trigger polarity]
        DCAM_TRIGPOL_POSITIVE = 0  # Positive polarity
        DCAM_TRIGPOL_NEGATIVE = 1  # Negative polarity

    class CCDDriveMode(IntEnum):
        # [CCD drive mode]
        DCAM_CCDDRVMODE_SUSPEND = 0  # Suspend
        DCAM_CCDDRVMODE_STANDBY = 1  # Standby

    class CCDOperatingMode(IntEnum):
        # [CCD operating mode]
        DCAM_OPMODE_DARKCURRENT = 0  # Low Dark Current Mode
        DCAM_OPMODE_SATURATION = 1  # High Saturation Charge Mode

    class LEDOperatingMode(IntEnum):
        # [LED operating mode]
        DCAM_LEDOPMODE_OFF = 0  # LED Off Mode
        DCAM_LEDOPMODE_ON = 1  # LED On Mode

    class DeviceInformationType(IntEnum):
        # [Device information type]
        DCAM_DEVINF_TYPE = 0  # Device type
        DCAM_DEVINF_SERIALNO = 1  # Serial number of device
        DCAM_DEVINF_VERSION = 2  # Device version

    class USBTransferRateType(IntEnum):
        # [USB transfer rate type]
        DCAM_TRANSRATE_USB11 = 0  # USB 1.1 standard
        DCAM_TRANSRATE_USB20 = 1  # USB 2.0 standard

    class ExternalTriggerOutputType(IntEnum):
        # [External trigger output type]
        DCAM_OUTMODE_NOTOUTPUT = 0  # Not output
        DCAM_OUTMODE_PLS_DT_PW = 1  # Output pulse (Delay time + Pulse width)
        DCAM_OUTMODE_PLS_ACCUM = 2  # Output pulse (Accumulation time)

    class ExternalTriggerOutputPolarity(IntEnum):
        # [External trigger output polarity]
        DCAM_OUTPOL_POSITIVE = 0  # Positive
        DCAM_OUTPOL_NEGATIVE = 1  # Negative

    class CCDType(IntEnum):
        # [CCD device type]
        DCAM_CCD_TYPE0 = 0  # 2068x70
        DCAM_CCD_TYPE1 = 1  # 2068x38
        DCAM_CCD_TYPE2 = 2  # 2068x22
        DCAM_CCD_TYPE3 = 3  # 1044x70
        DCAM_CCD_TYPE4 = 4  # 1044x38
        DCAM_CCD_TYPE5 = 5  # 1044x22
        DCAM_CCD_TYPE6 = 6  # 2118x120(for Evaluation)
        DCAM_CCD_TYPE7 = 7  # 2118x190(for Evaluation)
        DCAM_CCD_TYPE8 = 8  # 2068x1 : S11150
        DCAM_CCD_TYPE9 = 9  # 2068x1 : S11151
        DCAM_CCD_TYPE10 = 10  # 64x64 : G11097
        DCAM_CCD_TYPE11 = 11  # 2118x1(for Evaluation)
        DCAM_CCD_TYPE12 = 12  # 2068x1 : S11155
        DCAM_CCD_TYPE13 = 13  # 2068x1 : S11156
        DCAM_CCD_TYPE14 = 14  # 32x1 : G11135
        DCAM_CCD_TYPE15 = 15  # 64x1 : G11134
        DCAM_CCD_TYPE16 = 16  # 256x1 : G11477
        DCAM_CCD_TYPE17 = 17  # 256x1 : G11478
        DCAM_CCD_TYPE18 = 18  # 512x1 : G11135
        DCAM_CCD_TYPE19 = 19  # 512x1 : G11477
        DCAM_CCD_TYPE20 = 20  # 512x1 : G11478
        DCAM_CCD_TYPE21 = 21  # 562x1 : G11477, 8-256D(for Evaluation)
        DCAM_CCD_TYPE22 = 22  # 562x1 : G11135-512D(for Evaluation)
        DCAM_CCD_TYPE23 = 23  # 562x1 : G11477, 8-512D(for Evaluation)
        DCAM_CCD_TYPE24 = 24  # 256x1 : G11135
        DCAM_CCD_TYPE25 = 25  # 562x1 : G11135-256D(for Evaluation)
        DCAM_CCD_TYPE26 = 26  # 256x1 : G11608-256DA
        DCAM_CCD_TYPE27 = 27  # 512x1 : G11608-512DA
        DCAM_CCD_TYPE28 = 28  # 256x1 : G11620-256DA
        DCAM_CCD_TYPE29 = 29  # 512x1 : G11620-512DA
        DCAM_CCD_TYPE30 = 30  # 562x1 : G11608-256DA(for Evaluation)
        DCAM_CCD_TYPE31 = 31  # 562x1 : G11608-512DA(for Evaluation)
        DCAM_CCD_TYPE32 = 32  # 562x1 : G11620-256DA(for Evaluation)
        DCAM_CCD_TYPE33 = 33  # 562x1 : G11620-512DA(for Evaluation)
        DCAM_CCD_TYPE34 = 34  # 2056x1 : S11151
        DCAM_CCD_TYPE35 = 35  # 2106x1 : S11151(for Evaluation)
        DCAM_CCD_TYPE36 = 36  # 128x128 : G11097-0707
        DCAM_CCD_TYPE37 = 37  # 256x1 : G9203-256D
        DCAM_CCD_TYPE38 = 38  # 512x1 : G9204-512D
        DCAM_CCD_TYPE39 = 39  # 562x1 : G9204-256D(for Evaluation)
        DCAM_CCD_TYPE40 = 40  # 562x1 : G9204-512D(for Evaluation)
        DCAM_CCD_TYPE41 = 41  # 256x1 : G9494-256D
        DCAM_CCD_TYPE42 = 42  # 512x1 : G9494-512D
        DCAM_CCD_TYPE43 = 43  # 562x1 : G9494-256D(for Evaluation)
        DCAM_CCD_TYPE44 = 44  # 562x1 : G9494-512D(for Evaluation)
        DCAM_CCD_TYPE45 = 45  # 128x1 : G11620-128DA
        DCAM_CCD_TYPE46 = 46  # 256x1 : G11620-256DF
        DCAM_CCD_TYPE47 = 47  # 562x1 : G11620-128DA(for Evaluation)
        DCAM_CCD_TYPE48 = 48  # 562x1 : G11620-128DF(for Evaluation)
        DCAM_CCD_TYPE49 = 49  # 2128x1 : S11155-2048-02
        DCAM_CCD_TYPE50 = 50  # 2128x1 : S11156-2048-02
        DCAM_CCD_TYPE51 = 51  # Reserve
        DCAM_CCD_TYPE52 = 52  # 2178x1 : S11155-2048-02(for Evaluation)
        DCAM_CCD_TYPE53 = 53  # 2178x1 : S11156-2048-02(for Evaluation)
        DCAM_CCD_TYPE54 = 54  # Reserve

    class StandardTimeUnit(IntEnum):
        # [Standard time unit]
        DCAM_TIME_UNIT_TYPE1 = 0  # Trigger setting = [mSec], Pulse Out setting = [mSec]
        DCAM_TIME_UNIT_TYPE2 = 1  # Trigger setting = [uSec], Pulse Out setting = [uSec]
        DCAM_TIME_UNIT_TYPE3 = 2  # Trigger setting = [mSec], Pulse Out setting = [uSec]
        DCAM_TIME_UNIT_TYPE4 = 3  # Trigger setting = [Clock], Pulse Out setting = [Clock]

    class MPPMode(IntEnum):
        # [MPP mode]
        DCAM_CCDMPPMODE_OFF = 0  # Off
        DCAM_CCDMPPMODE_ON = 1  # On

    class ElectronicShutterMode(IntEnum):
        # [Electronic Shutter mode]
        DCAM_CCDESHUTTER_OFF = 0x00  # Off
        DCAM_CCDESHUTTER_ON = 0x01  # On

    nBitSize = c_int()

    def dcamGetLastError(self):
        # ============================================================================
        # DcamGetLastError =
        # Obtain the last-error code.
        # ---------------------------------------------------------------------------
        # [Argument]
        # None.
        # [Return values]
        # The latest error code is returned. See the error code table for
        # descriptions of error codes.
        # [Note]
        # ============================================================================

        return self.ErrorCode(self.lib.DcamGetLastError())

    def errHandler(self):
        print(inspect.stack()[1].function, self.dcamGetLastError())

    def dcamInitialize(self):
        # ============================================================================
        # DcamInitialize =
        # Initialize the library.
        # ---------------------------------------------------------------------------
        # [Argument]
        # None.
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # 1.	This function must first be run before running other functions.
        # 2.	An error is issued if the library has already been initialized.
        # 3.	Only one process can use this library.
        # ============================================================================

        if not self.lib.DcamInitialize():
            self.errHandler()

    def dcamUninitialize(self):
        # ============================================================================
        # DcamUninitialize =
        # Unload the library resources and close the device driver.
        # ---------------------------------------------------------------------------
        # [Argument]
        # None.
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # Call this function when quitting the program or the DCamLIB library is not needed.
        # ============================================================================

        if not self.lib.DcamUninitialize():
            self.errHandler()

    def dcamOpen(self):
        # ============================================================================
        # DcamOpen =
        # Open the device.
        # ---------------------------------------------------------------------------
        # [Argument]
        # None.
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # ============================================================================
        if not self.lib.DcamOpen():
            self.errHandler()

    def dcamClose(self):
        # ============================================================================
        # DcamClose =
        # Close the device.
        # ---------------------------------------------------------------------------
        # [Argument]
        # None.
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # ============================================================================
        if not self.lib.DcamClose():
            self.errHandler()

    def dcamGetDeviceState(self):
        # ============================================================================
        # DcamGetDeviceState =
        # Obtain the type of device state.
        # ---------------------------------------------------------------------------
        # [Argument]
        # pState   	:	# O: Specify the address of the variable where the type
        # of device state is to be stored. One of the
        # following values is obtained.
        # DCAM_DEVSTATE_NON   	: Non-connection, No device found
        # DCAM_DEVSTATE_DEVICE	: Non-connection, Device found
        # DCAM_DEVSTATE_NODEVICE	: Connection, No device found
        # DCAM_DEVSTATE_CONNECT	: Connection, Device found
        # DCAM_DEVSTATE_BOOT	: Connection, Device found(during the boot process)
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # ============================================================================

        p_state = c_int()

        if not self.lib.DcamGetDeviceState(byref(p_state)):
            self.errHandler()

        return self.DeviceState(p_state.value)

    def dcamSetCCDType(self, ccd_type: CCDType):
        # ============================================================================
        # DcamSetCCDType =
        # Set the CCD sensor type.
        # ---------------------------------------------------------------------------
        # [Argument]
        # nMode		:I	# : Specify the CCD sensor type from among the following types.
        # DCAM_CCD_TYPE0	:S10420-1106 (2068x70)
        # DCAM_CCD_TYPE3	:S10420-1006 (1044x70)
        # DCAM_CCD_TYPE10	:G11097 (64x64)
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # ============================================================================
        if not self.lib.DcamSetCCDType(ccd_type):
            self.errHandler()

    def dcamGetImageSize(self):
        # ============================================================================
        # DcamGetImageSize =
        # Obtain the width and height of image data to acquire from the USB Driver Board.
        # ---------------------------------------------------------------------------
        # [Argument]
        # pWidth		:	# O: Specify the address of the variable where the image
        # width is to be stored.
        # pHeight		:	# O: Specify the address of the variable where the image
        # height is to be stored.
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # ============================================================================
        n_width = c_int()
        n_height = c_int()
        if not self.lib.DcamGetImageSize(byref(n_width), byref(n_height)):  # Image size
            self.errHandler()

        return n_width.value, n_height.value

    def dcamSetExposureTime(self, n_time: int):
        # ============================================================================
        # DcamSetExposureTime =
        # Set the exposure time.
        # ---------------------------------------------------------------------------
        # [Argument]
        # nTime		:I	# : Specify an exposure time from 0 to 65535 [msec].
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # ============================================================================
        if 0 > n_time:
            n_time = 0
        elif n_time > 65535:
            n_time = 65535

        if not self.lib.DcamSetExposureTime(int(n_time)):
            self.errHandler()

    def dcamGetExposureTime(self):
        # ============================================================================
        # DcamGetExposureTime =
        # Obtain the exposure time.
        # ---------------------------------------------------------------------------
        # [Argument]
        # pTime		:	# O: Specify the address of the variable where the
        # currently setexposure time [msec] is to be stored.
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # ============================================================================
        p_time = c_int()
        if not self.lib.DcamGetExposureTime(byref(p_time)):
            self.errHandler()

        return p_time.value

    def dcamGetElectronicShutter(self):
        # ============================================================================
        # DcamGetElectronicShutter =
        # Get Electronic Shutter mode.
        # ---------------------------------------------------------------------------
        # [Argument]
        # pMode		:	# O: Specify the address of the variable
        # where the Electronic Shutter mode is to be stored.
        # DCAM_CCDESHUTTER_OFF	: Electronic Shutter mode is off.
        # DCAM_CCDESHUTTER_ON		: Electronic Shutter mode is on.
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]	C11165-01(X) correspondence.
        # ============================================================================
        p_mode = c_int()

        if not self.lib.DcamGetElectronicShutter(byref(p_mode)):
            self.errHandler()

        return self.ElectronicShutterMode(p_mode.value)

    def dcamSetElectronicShutter(self, n_mode: ElectronicShutterMode):
        # ============================================================================
        # DcamSetElectronicShutter =
        # Set Electronic Shutter mode.
        # ---------------------------------------------------------------------------
        # [Argument]
        # nMode		:I	# : Specify the Electronic Shutter mode.
        # DCAM_CCDESHUTTER_OFF	: Electronic Shutter mode is off.
        # DCAM_CCDESHUTTER_ON		: Electronic Shutter mode is on.
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]	C11165-01(X) correspondence.
        # ============================================================================
        if not self.lib.DcamSetElectronicShutter(n_mode):
            self.errHandler()

    def dcamGetMeasureDataCount(self):
        # ============================================================================
        # DcamGetMeasureDataCount =
        # Obtain the measurement line count.
        # ---------------------------------------------------------------------------
        # [Argument]
        # pCount		:	# O: Specify the address of the variable where the
        # currently set measurement line count is to be stored.
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # ============================================================================

        p_count = c_int()

        if not self.lib.DcamGetMeasureDataCount(byref(p_count)):
            self.errHandler()

        return p_count.value

    def dcamSetMeasureDataCount(self, n_count: int):
        # ============================================================================
        # DcamSetMeasureDataCount =
        # Set the measurement line count.
        # ---------------------------------------------------------------------------
        # [Argument]
        # nCount		:I	# : Specify an measurement line count from 1 to 100 [line].
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # When this function is run, the number of bytes per capture size may change.
        # Check the capture size with the DcamGetCaptureBytes function.
        # ============================================================================
        if 0 > n_count:
            n_count = 0
        elif n_count > 100:
            n_count = 100

        if not self.lib.DcamSetMeasureDataCount(n_count):
            self.errHandler()

    def dcamGetGain(self):
        # ============================================================================
        # DcamGetGain =
        # Obtain the gain.
        # ---------------------------------------------------------------------------
        # [Argument]
        # pGain		:	# O: Specify the address of the variable where the gain is
        # to be stored.
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # ============================================================================
        p_mode = c_int()

        if not self.lib.DcamGetGain(byref(p_mode)):
            self.errHandler()

        return p_mode.value

    def dcamSetGain(self, n_gain: int):
        # ============================================================================
        # DcamSetGain =
        # Set the gain.
        # ---------------------------------------------------------------------------
        # [Argument]
        # nGain		:I	# : Specify the gain value in the range from 1 to 10
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # ============================================================================
        if 0 > n_gain:
            n_gain = 1
        elif n_gain > 10:
            n_gain = 10

        if not self.lib.DcamSetGain(n_gain):
            self.errHandler()

    def dcamGetOffset(self):
        # ============================================================================
        # DcamGetOffset =
        # Obtain the offset.
        # ---------------------------------------------------------------------------
        # [Argument]
        # pOffset		:	# O: Specify the address of the variable where the offset is
        # to be stored.
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # ============================================================================
        p_offset = c_int()

        if not self.lib.DcamGetOffset(byref(p_offset)):
            self.errHandler()

        return p_offset.value

    def dcamSetOffset(self, n_offset: int):
        # ============================================================================
        # DcamSetOffset =
        # Set the offset.
        # ---------------------------------------------------------------------------
        # [Argument]
        # nOffset		:I	# : Specify the offset value in the range from 0 to 255
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # ============================================================================
        if 0 > n_offset:
            n_gain = 0
        elif n_offset > 255:
            n_gain = 255

        if not self.lib.DcamSetOffset(n_offset):
            self.errHandler()

    def dcamGetBinning(self):
        # ============================================================================
        # DcamGetBinning =
        # Obtain the binning.
        # ---------------------------------------------------------------------------
        # [Argument]
        # pBinning	:	# O: Specify the address of the variable where the
        # currently set
        # binning is to be stored. One of the following values
        # is obtained.
        # DCAM_BINNING_AREA	: Area Scaning
        # DCAM_BINNING_FULL	: Full line binning
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # ============================================================================
        p_binning = c_int()

        if not self.lib.DcamGetBinning(byref(p_binning)):
            self.errHandler()

        return self.BinningType(p_binning.value)

    def dcamSetBinning(self, n_mode: BinningType):
        # ============================================================================
        # DcamSetBinning =
        # Set the binning.
        # ---------------------------------------------------------------------------
        # [Argument]
        # nBinning	:I	# : Specify the binning. One of the following can be
        # specified.
        # DCAM_BINNING_AREA	: Area Scaning
        # DCAM_BINNING_FULL	: Full line binning
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # When this function is run, the number of bytes per capture size may change.
        # Check the capture size with the DcamGetCaptureBytes function.
        # ============================================================================
        if not self.lib.DcamSetBinning(n_mode):
            self.errHandler()

    def dcamGetTriggerMode(self):
        # ============================================================================
        # DcamGetTriggerMode =
        # Obtain the trigger mode.
        # ---------------------------------------------------------------------------
        # [Argument]
        # pMode		:	# O: Specify the address of the variable where the
        # currently set trigger mode
        # is to be stored. One of the following values is obtained.
        # DCAM_TRIGMODE_INT      				: Internal Mode
        # DCAM_TRIGMODE_EXT_EDGE				: External Trigger Edge Mode
        # DCAM_TRIGMODE_EXT_LEVEL				: External Trigger Level Mode
        #
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # ============================================================================

        p_mode = c_int()

        if not self.lib.DcamGetTriggerMode(byref(p_mode)):
            self.errHandler()

        return self.TriggerMode(p_mode.value)

    def dcamSetTriggerMode(self, n_mode: TriggerMode):
        # ============================================================================
        # DcamSetTriggerMode =
        # Set the trigger mode.
        # ---------------------------------------------------------------------------
        # [Argument]
        # nMode		:I	# : Specify the trigger mode. One of the following
        # can be specified.
        # C11287
        # DCAM_TRIGMODE_INT      	 			: Internal Mode
        # DCAM_TRIGMODE_EXT_EDGE   			: External Trigger Edge Mode
        # DCAM_TRIGMODE_EXT_LEVEL  			: External Trigger Level Mode
        #
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # ============================================================================
        if not self.lib.DcamSetTriggerMode(n_mode):
            self.errHandler()

    def dcamSetTriggerPolarity(self, n_mode: TriggerMode):
        # ============================================================================
        # DcamSetTriggerPolarity =
        # Set the trigger polarity.
        # ---------------------------------------------------------------------------
        # [Argument]
        # nPolarity	:I	# : Specify the trigger polarity. One of the following
        # can be specified.
        # DCAM_TRIGPOL_POSITIVE	: Positive
        # DCAM_TRIGPOL_NEGATIVE 	: Negative
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # ============================================================================
        if not self.lib.DcamSetTriggerPolarity(n_mode):
            self.errHandler()

    def dcamGetTriggerPolarity(self):
        # ============================================================================
        # DcamGetTriggerPolarity =
        # Obtain the trigger polarity.
        # ---------------------------------------------------------------------------
        # [Argument]
        # pPolarity	:	# O: Specify the address of the variable where the
        # currently set trigger polarity is to be stored.
        # One of the following values is obtained.
        # DCAM_TRIGPOL_POSITIVE	: Positive
        # DCAM_TRIGPOL_NEGATIVE 	: Negative
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # ============================================================================
        p_mode = c_int()
        if not self.lib.DcamGetTriggerPolarity(byref(p_mode)):
            self.errHandler()

        return self.TriggerPolarity(p_mode.value)

    def dcamSetTriggerPolarity(self, n_polarity: TriggerPolarity):
        # ============================================================================
        # DcamSetTriggerPolarity =
        # Set the trigger polarity.
        # ---------------------------------------------------------------------------
        # [Argument]
        # nPolarity	:I	# : Specify the trigger polarity. One of the following
        # can be specified.
        # DCAM_TRIGPOL_POSITIVE	: Positive
        # DCAM_TRIGPOL_NEGATIVE 	: Negative
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # ============================================================================
        if not self.lib.DcamSetTriggerPolarity(n_polarity):
            self.errHandler()

    def dcamGetBitPerPixel(self):

        p_bit = c_int()

        if not self.lib.DcamGetBitPerPixel(byref(p_bit)):
            self.errHandler()

        return self.BitPixel(p_bit.value)

    def dcamSetStandardTimeUnit(self, n_type: StandardTimeUnit):
        # ============================================================================
        # DcamSetStandardTimeUnit =
        # Set the standard time unit.
        # ---------------------------------------------------------------------------
        # [Argument]
        # nType		:I	# : Specify the standard unit type from among the following types.
        # DCAM_TIME_UNIT_TYPE1	: Trigger setting = [mSec], Pulse Out setting = [mSec]
        # DCAM_TIME_UNIT_TYPE2	: Trigger setting = [uSec], Pulse Out setting = [uSec]
        # DCAM_TIME_UNIT_TYPE3	: Trigger setting = [mSec], Pulse Out setting = [uSec]
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # ============================================================================
        if not self.lib.DcamSetStandardTimeUnit(n_type):
            self.errHandler()

    def dcamGetStandardTimeUnit(self):
        # ============================================================================
        # DcamGetStandardTimeUnit =
        # Obtain the standard time unit.
        # ---------------------------------------------------------------------------
        # [Argument]
        # pType		:	# O: Specify the address of the variable
        # where the standard time unit is to be stored.
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # ============================================================================
        n_type = c_int()
        if not self.lib.DcamGetStandardTimeUnit(byref(n_type)):
            self.errHandler()

        return self.StandardTimeUnit(n_type.value)

    def dcamWait(self, n_timeout):
        # ============================================================================
        # DcamWait =
        # Wait for image acquisition to complete.
        # ---------------------------------------------------------------------------
        # [Argument]
        # pStatus		:	# O: Specify the address of the variable where image
        # acquisition end status is to be stored. Whether
        # image acquisition is complete or not can be checked
        # by the value in this variable.
        # The value is one of the following:
        # DCAM_WAITSTATUS_COMPLETED	: Image acquisition is complete.
        # DCAM_WAITSTATUS_UNCOMPLETED	: Image acquisition is not complete.
        #
        # This may be set to NULL when "DCAM_WAIT_INFINITE" is
        # specified for "nTimeout".
        #
        # nTimeout	:I	# : Specify the length of timeout in milliseconds.
        # When "DCAM_WAIT_INFINITE" is specified here, the process
        # waits until image acquisition is finished.
        # When "0" is specified, control is returned immediately
        # after checking the status.
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # ============================================================================
        p_status = c_int()

        if not self.lib.DcamWait(byref(p_status), n_timeout):
            self.errHandler()

        return self.ImageAcquisition(p_status.value)

    def dcamStop(self):
        # ============================================================================
        # DcamStop =
        # Stop image acquisition.
        # ---------------------------------------------------------------------------
        # [Argument]
        # None.
        # [Return values]
        # If the function succeeds the return value is TRUE = 1.
        # If the function fails the return value is FALSE = 0.
        # To obtain detailed error information, use the DcamGetLastError function.
        # [Note]
        # ============================================================================
        if not self.lib.DcamStop():
            self.errHandler()

    def __init__(self, *args, **kwargs) -> None:
        super(Hamamatsu, self).__init__(*args, **kwargs)

        path_lib = path.join(path.dirname(path.abspath(__file__)), "lib", 'DCamUSB.dll')
        self.lib = cdll.LoadLibrary(path_lib)

        self.n_width, self.n_height = (0, 0)
        self.dark = np.empty(0)
        self.drop_beginning_lines=1
        self.setDefaultParameters()
        self.lib.DcamSetOperatingMode(1)
        # self.dcamInitialize()

    def __del__(self):
        self.close()
        self.dcamUninitialize()

    def setLed(self, stat):
        self.lib.DcamSetLEDOperatingMode(stat)


    def setDefaultParameters(self):
        self.dcamSetStandardTimeUnit(self.StandardTimeUnit.DCAM_TIME_UNIT_TYPE1)  # Standard time unit
        self.dcamSetBinning(self.BinningType.DCAM_BINNING_AREA)  # Full line binning
        self.dcamSetTriggerMode(self.TriggerMode.DCAM_TRIGMODE_INT)  # Trigger mode to INTERNAL
        self.dcamSetTriggerPolarity(self.TriggerPolarity.DCAM_TRIGPOL_POSITIVE)  # Trigger polarity
        self.dcamSetExposureTime(1)  # Exposure Time
        self.dcamSetGain(1)  # Gain 1
        self.dcamSetOffset(100)  # Offset 10
        self.dcamSetMeasureDataCount(1)  # Acquisition frame count

        self.n_width, self.n_height = self.dcamGetImageSize()
        self.dark = np.empty(self.n_width * (self.n_height-self.drop_beginning_lines))





if __name__ == "__main__":
    cam = Hamamatsu()
    cam.open()
    cam.setDefaultParameters()

    print(len(cam.getMeasure()[:]))

    cam.close()
    cam.dcamUninitialize()
