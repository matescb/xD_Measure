
	# ============================================================================
	# DcamOpenIntendDev =
	# Open the device.
	# ---------------------------------------------------------------------------
	# [Argument]
	# nDevIndex : To open device index of intended.
	# If this index is "-1", this function open the device select dialog.
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# ============================================================================
_DCamUSBEXPORT BOOL _DCamUSBSTDCALL DcamOpenIntendDev( INT nDevIndex );


	# ============================================================================
	# DcamGetCaptureBytes =
	# Obtain the number of bytes of one frame.
	# ---------------------------------------------------------------------------
	# [Argument]
	# pBytes  	:	# O: Specify the address of the variable where the
	# number of bytes of one frame is to be stored.
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamGetCaptureBytes( INT* pBytes );

	# ============================================================================
	# DcamGetTotalCaptureBytes =
	# Obtain the total number of bytes per capture size.
	# ---------------------------------------------------------------------------
	# [Argument]
	# pBytes  	:	# O: Specify the address of the variable where the total
	# number of bytes per capture size is to be stored.
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamGetTotalCaptureBytes( INT* pBytes );

	# ============================================================================
	# DcamCapture =
	# Start to acquire one image from the USB Driver Board.
	# ---------------------------------------------------------------------------
	# [Argument]
	# pImageBuff	:	# O: Specify the start address in the buffer where image
	# data is to be stored.
	# nBuffSize	:I	# : Specify the buffer size (number of bytes).
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# 1. This function issues an instruction to start image acquisition.
	# Since image acquisition is not complete even when this function ends,
	# use the DcamWait function to check whether image acquisition is complete.
	# 2. The necessary buffer size can be obtained with the DcamGetCaptureBytes function.
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamCapture( LPVOID pImageBuff, INT nBuffSize );
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamCaptureEx( WORD* pImageBuff, INT nBuffSize );

	# ============================================================================
	# DcamCaptureReverseX =
	# Start to acquire one image from the USB Driver Board.
	# The X axis of the acquired image data is reversed.
	# ---------------------------------------------------------------------------
	# [Argument]
	# pImageBuff	:	# O: Specify the start address in the buffer where image
	# data is to be stored.
	# nBuffSize	:I	# : Specify the buffer size (number of bytes).
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# 1. This function issues an instruction to start image acquisition.
	# Since image acquisition is not complete even when this function ends,
	# use the DcamWait function to check whether image acquisition is complete.
	# 2. The necessary buffer size can be obtained with the DcamGetCaptureBytes function.
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamCaptureReverseX( LPVOID pImageBuff, INT nBuffSize );


	# ============================================================================
	# DcamStopEx =
	# This function stop image acquisition, and wait the reset of acquiring process
	# in camera will be done.
	# ---------------------------------------------------------------------------
	# [Argument]
	# None.
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# ============================================================================*	#
_DCamUSBEXPORT BOOL _DCamUSBSTDCALL DcamStopEx( VOID );


ImageAcquisition
	# ============================================================================
	# DcamWait =
	# Wait for image acquisition to complete.
	# ---------------------------------------------------------------------------
	# [Argument]
	# pStatus		:	# O: Specify the address of the variable where image
	# acquisition end status is to be stored. Whether
	# image acquisition is complete or not can be checked
	# by the value in this variable.
	# The value is one of the following:
	# DCAM_WAITSTATUS_COMPLETED	: Image acquisition is complete.
	# DCAM_WAITSTATUS_UNCOMPLETED	: Image acquisition is not complete.
	#
	# This may be set to NULL when "DCAM_WAIT_INFINITE" is
	# specified for "nTimeout".
	#
	# nTimeout	:I	# : Specify the length of timeout in milliseconds.
	# When "DCAM_WAIT_INFINITE" is specified here, the process
	# waits until image acquisition is finished.
	# When "0" is specified, control is returned immediately
	# after checking the status.
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamWait( DWORD* pStatus, INT nTimeout = 0 );



	# ============================================================================
	# DcamSetDriveMode =
	# Set the CCD drive mode.
	# ---------------------------------------------------------------------------
	# [Argument]
	# nMode		:I	# : Specify the CCD drive mode.
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamSetDriveMode( INT  nMode, INT nTime  );

	# ============================================================================
	# DcamGetDriveMode =
	# Obtain the CCD drive mode.
	# ---------------------------------------------------------------------------
	# [Argument]
	# pMode		:	# O: Specify the address of the variable where
	# the CCD drive mode is to be stored.
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamGetDriveMode( INT* pMode );



_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamSetCCDType( INT  nType );

	# ============================================================================
	# DcamGetCCDType =
	# Obtain the CCD sensor type.
	# ---------------------------------------------------------------------------
	# [Argument]
	# pType		:	# O: Specify the address of the variable where the
	# currently set the CCD sensor type is to be stored.
	# One of the following values is obtained.
	# DCAM_CCD_TYPE0	:S10420-1106 (2068x70)
	# DCAM_CCD_TYPE3	:S10420-1006 (1044x70)
	# DCAM_CCD_TYPE10	:G11097 (64x64)
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamGetCCDType( INT* pType );

	# ============================================================================
	# DcamSetOperatingMode =
	# Set the CCD operating mode.
	# ---------------------------------------------------------------------------
	# [Argument]
	# nMode		:I	# : Specify the CCD Operating from among the following modes.
	# DCAM_OPMODE_DARKCURRENT	: Low Dark Current Mode
	# DCAM_OPMODE_SATURATION	: High Saturation Charge Mode
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamSetOperatingMode( INT nMode );

	# ============================================================================
	# DcamGetOperatingMode =
	# Obtain the CCD operating mode.
	# ---------------------------------------------------------------------------
	# [Argument]
	# pType		:	# O: Specify the address of the variable where the
	# currently set the CCD Operating mode is to be stored.
	# One of the following values is obtained.
	# DCAM_OPMODE_DARKCURRENT	: Low Dark Current Mode
	# DCAM_OPMODE_SATURATION	: High Saturation Charge Mode
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamGetOperatingMode( INT* pMode );

	# ============================================================================
	# DcamSetLEDOperatingMode =
	# Set the LED operating mode.
	# ---------------------------------------------------------------------------
	# [Argument]
	# nMode		:I	# : Specify the LED Operating from among the following modes.
	# DCAM_LEDOPMODE_OFF	: Off Mode
	# DCAM_LEDOPMODE_ON	: On Mode
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamSetLEDOperatingMode( INT nMode );

	# ============================================================================
	# DcamGetLEDOperatingMode =
	# Obtain the LED operating mode.
	# ---------------------------------------------------------------------------
	# [Argument]
	# pType		:	# O: Specify the address of the variable where the
	# currently set the LED Operating mode is to be stored.
	# One of the following values is obtained.
	# DCAM_LEDOPMODE_OFF	: Off Mode
	# DCAM_LEDOPMODE_ON	: On Mode
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamGetLEDOperatingMode( INT* pMode );

	# ============================================================================
	# DcamSetOutPulse =
	# Set The information on an external output
	# ---------------------------------------------------------------------------
	# [Argument]
	# nMode		:I	# : Specifies the output mode to set up
	# DCAM_OUTMODE_NOTOUTPUT   ： No Output
	# DCAM_OUTMODE_PLS_DT_PW   ： Output(Delay Time + Pulse width)
	# DCAM_OUTMODE_PLS_ACCUM   ： Output(Accumulation time)
	# nPolarity	:I	# : The polarity to set up is specified.
	# DCAM_OUTPOL_POSITIVE	： Positive
	# DCAM_OUTPOL_NEGATIVE	： Negative
	# nDelayTime	:I	# : The delay time to set up is specified.
	# nPulseWidth	:I	# : The pulse width to set up is specified.
	#
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamSetOutPulse(INT nMode, INT nPolarity,
									INT nDelayTime, INT nPulseWidth);

	# ============================================================================
	# DcamGetOutPulse =
	# Get The information on an external output
	# ---------------------------------------------------------------------------
	# [Argument]
	# pMode			:	# O: Pointer to variable to store the output mode
	# DCAM_OUTMODE_NOTOUTPUT   ： No Output
	# DCAM_OUTMODE_PLS_DT_PW   ： Output(Delay Time + Pulse width)
	# DCAM_OUTMODE_PLS_ACCUM   ： Output(Accumulation time)
	# pPolarity		:	# O: Pointer to variable to store the output polarity
	# DCAM_OUTPOL_POSITIVE	： Positive
	# DCAM_OUTPOL_NEGATIVE	： Negative
	# pDelayTime		:	# O: Pointer to variable to store the delay time
	# pPulseWidth		:	# O: Pointer to variable to store the pulse width
	#
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamGetOutPulse(INT* pMode, INT* pPolarity,
									INT* pDelayTime, INT* pPulseWidth);

	# ============================================================================
	# DcamLoadParameters =
	# Load parameters to the device.
	# ---------------------------------------------------------------------------
	# [Argument]
	# nTimeout	:I	# : Specify the length of timeout in milliseconds.
	# Please set one or more values.
	# When "0" is specified, processing is carried out
	# by the standard timeout.
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamLoadParameters( INT nTimeout );

	# ============================================================================
	# DcamStoreParameters =
	# Store parameters to the device.
	# ---------------------------------------------------------------------------
	# [Argument]
	# None.
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamStoreParameters( VOID );

	# ============================================================================
	# DcamGetVersion =
	# Obtain the library version number, in string format.
	# ---------------------------------------------------------------------------
	# [Argument]
	# szVersion	:	# O: Specify the start address in the character string
	# buffer where the version of the library is to be stored.
	# nBuffSize	:I	# : Specify the buffer size (number of bytes).
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamGetVersion( char* szVersion, INT nBuffSize );

	# ============================================================================
	# DcamGetDriverVersion =
	# Obtain the driver version number, in string format.
	# ---------------------------------------------------------------------------
	# [Argument]
	# szVersion	:	# O: Specify the start address in the character string
	# buffer where the version of the driver is to be stored.
	# nBuffSize	:I	# : Specify the buffer size (number of bytes).
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamGetDriverVersion( char* szVersion, INT nBuffSize );

	# ============================================================================
	# DcamGetFirmwareVersion =
	# Obtain the firmware version number, in string format.
	# ---------------------------------------------------------------------------
	# [Argument]
	# szVersion	:	# O: Specify the start address in the character string
	# buffer where the version of the firmware is to be stored.
	# nBuffSize	:I	# : Specify the buffer size (number of bytes).
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamGetFirmwareVersion( char* szFirmVersion, INT nBuffSize );

	# ============================================================================
	# DcamGetDeviceInformation =
	# Obtain the device information, in string format.
	# ---------------------------------------------------------------------------
	# [Argument]
	# nType		:I	# : Specify the Information Type from among the following types.
	# DCAM_DEVINF_TYPE		: Device type
	# DCAM_DEVINF_SERIALNO	: Serial number of device
	# DCAM_DEVINF_VERSION		: Device version
	# szBuff		:	# O: Specify the start address in the character string buffer
	# where the information of device is to be stored.
	# nBuffSize	:I	# : Specify the buffer size (number of bytes).
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamGetDeviceInformation( INT nType, char* szBuff, INT nBuffSize );

	# ============================================================================
	# DcamGetTransferRateType =
	# Obtain the USB transfar rate type.
	# ---------------------------------------------------------------------------
	# [Argument]
	# pType   	:	# O: Specify the address of the variable where the USB
	# transfar rate type is to be stored. One of the
	# following values is obtained.
	# DCAM_TRANSRATE_USB11	: USB 1.1 standard
	# DCAM_TRANSRATE_USB20	: USB 2.0 standard
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamGetTransferRateType( INT* pType );
DcamSetMeasureDataCount

	# ============================================================================
	# DcamSetOverClock =
	# Set the over clock.
	# ---------------------------------------------------------------------------
	# [Argument]
	# nClock		:I	# : Specify the length of over clock in clock (number of MCLK).
	#
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamSetOverClock(INT nClock);

	# ============================================================================
	# DcamGetOverClock =
	# Obtain the over clock.
	# ---------------------------------------------------------------------------
	# [Argument]
	# pClock		:	# O: Specify the length of over clock in clock(number of MCLK).
	#
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamGetOverClock(INT *pClock);

	# ============================================================================
	# DcamSetMPPMode =
	# Set MPP mode.
	# ---------------------------------------------------------------------------
	# [Argument]
	# nMode		:I	# : Specify the MPP mode.
	# DCAM_CCDMPPMODE_OFF	: MPP mode is off.
	# DCAM_CCDMPPMODE_ON	: MPP mode is on.
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]	C11165 correspondence.
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamSetMPPMode( INT nMode ) ;

	# ============================================================================
	# DcamGetMPPMode =
	# Get MPP mode.
	# ---------------------------------------------------------------------------
	# [Argument]
	# pMode		:	# O: Specify the address of the variable
	# where the MPP mode is to be stored.
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]	C11165 correspondence.
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamGetMPPMode( INT* pMode ) ;

	# ============================================================================
	# DcamSetLineTime =
	# Set line time.
	# ---------------------------------------------------------------------------
	# [Argument]
	# nTime		:I	# : Specify the line time.
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]	C11160,C11165 correspondence.
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamSetLineTime( INT nTime ) ;

	# ============================================================================
	# DcamGetLineTime =
	# Get line time.
	# ---------------------------------------------------------------------------
	# [Argument]
	# pTime		:	# O: Specify the address of the variable
	# where the line time is to be stored.
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]	C11160,C11165 correspondence.
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamGetLineTime( INT* pTime ) ;

	# ============================================================================
	# DcamSetIntegralCapacity =
	# Set integral capacity.
	# ---------------------------------------------------------------------------
	# [Argument]
	# nType		:I	# : Specify the type of integral capacity.
	# 0-7 -> G11477,G11478
	# 0-3 -> G11135
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]	C11512,C11513 correspondence.
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamSetIntegralCapacity(INT nType);

	# ============================================================================
	# DcamGetIntegralCapacity =
	# Get integral capacity.
	# ---------------------------------------------------------------------------
	# [Argument]
	# pType		:	# O: Specify the address of the variable
	# where the integral capacity is to be stored.
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]	C11512,C11513 correspondence.
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamGetIntegralCapacity(INT* pType);

	# ============================================================================
	# DcamSetSensorSignalPulseWidth =
	# Set pulse width of sensor signal.
	# ---------------------------------------------------------------------------
	# [Argument]
	# nSignalSensor	:I	# : Specify the signal type.	* only '0'
	# 0 : TG
	#
	# nWidth			:I	# : Specify the pulse width.  * "usec"
	#
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]	C11165-01(X) correspondence.
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamSetSensorSignalPulseWidth( INT nSignalSensor, INT nWidth );

	# ============================================================================
	# DcamGetSensorSignalPulseWidth =
	# Get pulse width of sensor signal.
	# ---------------------------------------------------------------------------
	# [Argument]
	# nSignalSensor	:I	# : Specify the signal type.	* only '0'
	# 0 : TG
	#
	# pWidth			:	# O: Specify the address of the variable
	# where the Pulse Width is to be stored.
	#
	# [Return values]
	# If the function succeeds the return value is TRUE = 1.
	# If the function fails the return value is FALSE = 0.
	# To obtain detailed error information, use the DcamGetLastError function.
	# [Note]	C11165-01(X) correspondence.
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamGetSensorSignalPulseWidth( INT nSignalSensor, INT* pWidth );

	# ============================================================================
	# DcamDeviceIsAlive =
	# Get Device Connection Status.
	# ---------------------------------------------------------------------------
	# [Argument]
	# [Return values]
	# If the device alived(connect) : TRUE = 1.
	# If the device is not connect : FALSE = 0.
	# [Note]
	# ============================================================================
_DCamUSBEXPORT  BOOL _DCamUSBSTDCALL DcamDeviceIsAlive(VOID);
