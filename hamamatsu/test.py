
from hamamatsu import Hamamatsu
import png

from ctypes import *
import matplotlib.pyplot as plt
import numpy as np

cam = Hamamatsu()

cam.open()
cam.setDefaultParameters()

name = "2"

drk = False
# 308 um na řádek
cam.lib.DcamSetOperatingMode(1)

scan = np.array(cam.getMeasure()[:])

test_array = np.reshape(scan, (-1, cam.nWidth.value))
plt.imshow(test_array, cmap='jet')
plt.tight_layout()

plt.savefig(f"{name}.jpg", format="jpg", dpi=400)
np.savetxt(f"{name}.csv", test_array.astype(int), fmt='%i', delimiter=";")
png.from_array(test_array.astype(np.uint16), "L").save(f"{name}.png")

print(f"Min {test_array.min()}, max {test_array.max()}, mean {test_array.mean()}")

