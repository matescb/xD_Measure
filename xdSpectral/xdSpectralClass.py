from xdDevice import xdDevice

class xdSpectral(xdDevice):
    binSize=30

    def __init__(self, name) -> None:
        super().__init__(name)

    def __del__(self):
        super(xdSpectral, self).__del__()

    def open(self):
        super().open()

    def close(self):
        super().close()

    def start(self):
        pass

    def stop(self):
        pass

    def resume(self):
        pass

    def read(self):
        pass

    def measureTimed(self):
        pass
