import threading
from xdSpectral.xdSpectralClass import xdSpectral
from time import sleep

class SpectralManager:
    DS=  {}
    spectrals = {}
    callbacks = {}
    measureTime=0
    _th = {}

    class LiveThread(threading.Thread):
        def __init__(self, dev: xdSpectral, period):
            threading.Thread.__init__(self)
            self._stop_event = threading.Event()
            self.dev = dev
            self.period = period

        def stop(self):
            self._stop_event.set()

        def stopped(self):
            return self._stop_event.is_set()

        def run(self):
            while not self.stopped():
                self.dev.read()
                sleep(self.period)

    def __init__(self) -> None:
        super().__init__()

    def openDevice(self, dev):
        self.spectrals[dev.name]=dev
        dev.open()
        self.callbacks[dev.name]=[]

    def addMeasured(self,data, name):
        self.DS[name]=data
        for f in self.callbacks[name]:
            f()

    def addCallback(self, fce, name):
        self.callbacks[name].append(fce)

    def startLive(self, name, period):
        if name in list(self._th):
            print(f"Live for {name} is already running")
        else:
            print(f"starting live for {name}")
            self._th[name]=self.LiveThread(self.spectrals[name], period)
            self._th[name].start()

    def stopLive(self, name, *args, **kwargs):

        if name in list(self._th):
            self._th[name].stop()
            self.spectrals[name].stop()
            del self._th[name]
            print(f"stopping live for {name}")
        else:
            print(f"Live for {name} is already stopped")

    def measureTimer(self, name, timer, period):
        self.spectrals[name].start()
        self.startLive(name,period)
        t = threading.Timer(timer,self.stopLive, args=[name])
        t.start()

    def saveToFolder(self, path):
        for d in self.DS:
            self.DS[d].save(path, d)

    def measureAll(self):
        for s in list(self.spectrals):
            self.measureTimer(s, self.measureTime, 0.3)