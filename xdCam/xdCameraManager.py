from typing import Type

from standa.standa import Standa
import serial.tools.list_ports
import sys
import glob
import serial
from standa.standa import get_probe
from time import sleep


class CameraManager:
    DS={}
    cameras = {}
    callbacks={}

    def __init__(self) -> None:
        super().__init__()

    def openCamera(self, cam):
        self.cameras[cam.name]=cam
        cam.open()
        self.callbacks[cam.name]=[]

    def measureAll(self):
        for cam in self.cameras:
            self.cameras[cam].getMeasure()


    def addMeasured(self,data, name):
        self.DS[name]=data
        for f in self.callbacks[name]:
            f()

    def addCallback(self, fce, name):
        self.callbacks[name].append(fce)

    def saveToFolder(self, path):
        for d in self.DS:
            self.DS[d].save(path, d)

    def closeAll(self):
        for cam in self.cameras:
            self.cameras[cam].close()

    def __del__(self):
        self.closeAll()

