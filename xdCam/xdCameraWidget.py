from PyQt6.QtWidgets import QApplication, QFrame
from PyQt6 import QtCore
import sys

if __name__ == "__main__":
    from xdCameraWidget_ui import Ui_Form
else:
    from .xdCameraWidget_ui import Ui_Form

import numpy as np
import pyqtgraph as pg

from hamamatsu.hamamatsu import xdHamamatsu
from time import sleep

from xdCore import cmg

'''
D:\DP\QT_playing\venv\Scripts\pyuic6.exe D:\DP\QT_playing\xdCam\xdCameraWidget_ui.ui -o D:\DP\QT_playing\xdCam\xdCameraWidget_ui.py
'''
class CameraWidget(Ui_Form, QFrame):

    # your logic here
    def __init__(self,frame,name):
        super(CameraWidget, self).__init__()
        self.cam=cmg.cameras[name]
        self.setupUi(self)

        self.pushButton_capture.clicked.connect(self.capture)
        self.pushButton_darkframe.clicked.connect(self.getDarkFrame)

        colors = [
            (0, 0, 0),
            (45, 5, 61),
            (84, 42, 55),
            (150, 87, 60),
            (208, 171, 141),
            (255, 255, 255)
        ]
        cmap = pg.ColorMap(pos=np.linspace(0.0, 1, 6), color=colors)
        self.graphicsView.setColorMap(cmap)

     #   self.myThread = self.YourThreadName(self)
     #   self.myThread.start()

        # Start up with an ROI
        # self.graphicsView.ui.roiBtn.setChecked(True)
        # self.graphicsView.roiClicked()
        self.graphicsView.ui.roiBtn.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.graphicsView.ui.menuBtn.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.thread = {}
        cmg.addCallback(self.draw, self.cam.name)


    def getDarkFrame(self):
        self.cam.setDarkImage(10)

    def draw(self):
        self.graphicsView.setImage(cmg.DS[self.cam.name].data["data"], xvals=np.linspace(1., 3., cmg.DS[self.cam.name].data["data"].shape[0]))


    def saveMeasured(self):
        self.mes.saveMeasured(self.fileName.text())

    def setCamera(self):
        self.cam.exposure=self.exposure.value()
        self.cam.gain=self.gain.value()

        self.exposure.setValue(self.cam.exposure)
        self.gain.setValue(self.cam.gain)

    def capture(self):
        self.setCamera()
        self.cam.getMeasure(average=1)#self.averaging.value())

    def scan(self):
        self.setCamera()
        self.mes.measureScan(self.doubleSpinBox_start.value(), self.doubleSpinBox_end.value(), average=self.averaging.value())

class ThreadClass(QtCore.QThread):
    any_signal = QtCore.pyqtSignal(int)

    def __init__(self, parent=None, index=0):
        super(ThreadClass, self).__init__(parent)
        self.index = index
        self.is_running = True

    def run(self):
        print('Starting thread...', self.index)
        cnt = 0
        while (True):
            cnt += 1
            if cnt == 99: cnt = 0
            sleep(0.01)
            self.any_signal.emit(cnt)

    def stop(self):
        self.is_running = False
        print('Stopping thread...', self.index)
        self.terminate()


if __name__ == "__main__":
    app = QApplication([])

    cmg.openCamera(xdHamamatsu("Hamamatsu"))
    window = CameraWidget(app,name="Hamamatsu")

    window.show()
    sys.exit(app.exec())
