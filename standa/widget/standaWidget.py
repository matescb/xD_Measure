from PyQt6.QtWidgets import QApplication, QFrame, QHBoxLayout

from PyQt6 import QtCore, uic
import sys
if __name__ == "__main__":
    from standaWidget_ui import Ui_Form
else:
    from .standaWidget_ui import Ui_Form
from xdCore import stg, cfg
import numpy as np
from time import sleep
import re
# D:\DP\QT_playing\venv\Scripts\pyuic6.exe D:\DP\QT_playing\standa\widget\standaWidget_ui.ui -o D:\DP\QT_playing\standa\widget\standaWidget_ui.py

class standaWidget(QFrame,Ui_Form):
    class YourThreadName(QtCore.QThread):

        def __init__(self, myUI):
            QtCore.QThread.__init__(self)
            self.myUI = myUI

        def __del__(self):
            self.wait()

        def run(self):
            while True:
                position = self.myUI.lin.position
                self.myUI.pushButton_position.setText("{0:5.3f} ".format(position)+ str(self.myUI.unit))
                self.myUI.horizontalSlider_position.setValue(int(position))
                sleep(0.05)
        def stop(self):
            self.terminate()


    # your logic here
    def __init__(self,form,lin):
        super(standaWidget, self).__init__()
        self.setupUi(self)

        self.lin=stg.open(lin)

        self.lineEdit_position.hide()
        self.pushButton_positionConfirm.hide()
        self.pushButton_positionReset.hide()

        self.unit = self.lin.unitStr.replace(r"/o","°")

        self.pushButton_moveTo.clicked.connect(self.goMoveTo)
        self.pushButton_shiftOn.clicked.connect(self.goShiftOn)
        self.pushButton_stop.clicked.connect(self.goStop)

        self.pushButton_home.clicked.connect(self.goHome)

        self.pushButton_goRight.pressed.connect(self.goRight)
        self.pushButton_goRight.released.connect(self.goStop)

        self.pushButton_goLeft.pressed.connect(self.goLeft)
        self.pushButton_goLeft.released.connect(self.goStop)

        self.pushButton_position.clicked.connect(self.positionChange)

        self.myThread = self.YourThreadName(self)
        self.myThread.start()

        self.pushButton_positionReset.clicked.connect(self.positionChangeReset)
        self.pushButton_positionConfirm.clicked.connect(self.positionChangeConfirm)
        self.pushButton_close.clicked.connect(self.close)

        task_h_box = QHBoxLayout()
        self.setLayout(task_h_box)

        self.label_stageName.setText(f"Stage {self.lin.name} control")

        self.doubleSpinBox_moveTo.setSuffix(" "+ self.unit)
        self.pushButton_position.setText("0.00000 "+ self.unit)
        self.doubleSpinBox_shiftOn.setSuffix(" "+ self.unit)

    def close(self) :
        self.myThread.terminate()
        stg.close(self.lin.name)
        super().close()

    def positionChange(self):
        self.lineEdit_position.setText(self.pushButton_position.text())
        self.lineEdit_position.setMinimumWidth(self.pushButton_position.width())
        self.lineEdit_position.setMinimumHeight(self.pushButton_position.height())

        self.lineEdit_position.setMaximumWidth(self.pushButton_position.width())
        self.lineEdit_position.setMaximumHeight(self.pushButton_position.height())

        self.lineEdit_position.show()
        self.pushButton_position.hide()
        self.pushButton_goRight.hide()

        self.pushButton_positionConfirm.show()
        self.pushButton_positionReset.show()

        self.pushButton_positionConfirm.setFocus(QtCore.Qt.FocusReason.TabFocusReason)
        self.pushButton_positionConfirm.setChecked(True)
        self.pushButton_positionConfirm.setAutoDefault(True)

    def positionChangeReset(self):
        position = self.lin.position
        self.pushButton_position.setText("{0:5.3f} ".format(position)+ str(self.unit))

        self.lineEdit_position.hide()
        self.pushButton_position.show()
        self.pushButton_positionConfirm.hide()
        self.pushButton_positionReset.hide()
        self.pushButton_goRight.show()

    def positionChangeConfirm(self):

        position = self.lineEdit_position.text()
        position = position.replace(",", ".")
        position = re.findall("\d+", position)
        if len(position) == 1:
            self.lin.position=int(position[0])
        elif len(position) == 2:
            self.lin.position=float(f"{position[0]}.{position[1]}")

        self.positionChangeReset()

    def goMoveTo(self):
        self.lin.go_move(self.doubleSpinBox_moveTo.value())

    def goShiftOn(self):
        self.lin.go_shift(self.doubleSpinBox_shiftOn.value())

    def goStop(self):
        self.lin.go_stop()

    def goRight(self):
        self.lin.go_right()

    def goLeft(self):
        self.lin.go_left()

    def goHome(self):
        self.lin.go_home()
        class waitHome(QtCore.QThread):

            def __init__(self, myUI):
                QtCore.QThread.__init__(self)
                self.myUI = myUI

            def run(self):
                self.myUI.lin.wait_for_stop()

                if cfg.has_option(self.myUI.lin.name, "homePosition"):
                    homePosition = cfg[self.myUI.lin.name]["homePosition"]
                else:
                    homePosition = 0


                self.myUI.lin.position=float(homePosition)
        self.waitHome = waitHome(self)
        self.waitHome.start()


if __name__ == "__main__":

    app = QApplication([])
    window = standaWidget(app, "Pricne")

    window.show()
    sys.exit(app.exec())
