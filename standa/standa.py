from ctypes import *
import os
import sys
import platform

#Is it used?
#import netifaces
#import getpass

from pathlib import Path

cur_dir = os.path.abspath(os.path.dirname(__file__))  # Specifies the current directory.
ximc_dir = os.path.join(cur_dir, "ximc")  # Formation of the directory name with all dependencies. 
sys.path.append(ximc_dir)  # add pyximc.py wrapper to python path

if platform.system() == "Windows":
    # Determining the directory with dependencies for windows depending on the bit depth.
    arch_dir = "win64" if "64" in platform.architecture()[0] else "win32"  #
    libdir = os.path.join(ximc_dir, arch_dir)

    if sys.version_info >= (3, 8):
        os.add_dll_directory(libdir)
    else:
        os.environ['PATH'] = libdir + os.pathsep + os.environ['PATH']
        pass#os.environ["LD_LIBRARY_PATH"] =libdir

if __name__ == "__main__":
    from ximc.pyximc import *
else:
    from .ximc.pyximc import *


def get_probe(port):
    uri = f"xi-com:\\\\.\{port}".encode()
    if lib.probe_device(uri) == 0:
        return True, Standa(port).name
    else:
        return False, ""

class Standa:
    port = ""

    def __init__(self, port) -> None:
        """

        :rtype: object
        """
        self.port=port
        open_name = f"xi-com:\\\\.\{port}".encode()

     #   lib.ximc_fix_usbser_sys(open_name)
        print("\nOpen device " + repr(open_name))
        self.device = lib.open_device(open_name)

        if self.device <= 0:
            print("Error open device ")
            exit(1)
        else:
            print("Device id: " + repr(self.device))

        # Create engine settings structure
        self.engineParameters = engine_settings_t()
        lib.get_engine_settings(self.device, byref(self.engineParameters))

        self.stageParameters = stage_settings_t()
        lib.get_stage_settings(self.device, byref(self.stageParameters))

        if self.stageParameters.LeadScrewPitch==0:
            self.stageParameters.LeadScrewPitch=1


        # Create user unit settings structure
        self.user_unit = calibration_t()
        self.user_unit.A = 1

        self.user_unit.MicrostepMode = 9


        self.set_user_unit_mode(self.stageParameters.LeadScrewPitch / self.engineParameters.StepsPerRev)

    def close(self):
        lib.close_device(byref(cast(self.device, POINTER(c_int))))

    def __del__(self):
        self.close()

    @property
    def name(self):
        name = stage_name_t()

        lib.get_stage_name(self.device, byref(name))
        return name.PositionerName.decode("utf-8")

    @property
    def unitStr(self):
        return self.stageParameters.Units.decode("utf-8")

    def get_info(self):
        """
        Reading information about the device.
        
        :param lib: structure for accessing the functionality of the libximc library.
        :param device_id: device id.
        """

        print("\nGet device info")
        x_device_information = device_information_t()
        result = lib.get_device_information(self.device, byref(x_device_information))
        print("Result: " + repr(result))
        if result == Result.Ok:
            print("Device information:")
            print(" Manufacturer: " +
                  repr(string_at(x_device_information.Manufacturer).decode()))
            # print(" ManufacturerId: " +
            #        repr(string_at(x_device_information.ManufacturerId).decode()))
            # print(" ProductDescription: " +
            #        repr(string_at(x_device_information.ProductDescription).decode()))
            print(" Hardware version: " + repr(x_device_information.Major) + "." + repr(x_device_information.Minor) +
                  "." + repr(x_device_information.Release))
            # print(" Major: " + repr(x_device_information.Major))
            # print(" Minor: " + repr(x_device_information.Minor))
            # print(" Release: " + repr(x_device_information.Release))

    def get_status(self):
        """
        A function of reading status information from the device

        You can use this function to get basic information about the device status.
        
        :param lib: structure for accessing the functionality of the libximc library.
        :param device_id:  device id.
        """

        print("\nGet status")
        x_status = status_t()
        result = lib.get_status(self.device, byref(x_status))
        print("Result: " + repr(result))
        if result == Result.Ok:
            print("Status.Ipwr: " + str(int(repr(x_status.Ipwr)) / 1000) + " A")
            print("Status.Upwr: " + str(int(repr(x_status.Upwr)) / 100) + " V")
            print("Status.Iusb: " + repr(x_status.Iusb))
            print("Status.Flags: " + repr(hex(x_status.Flags)))
            print("Status.Position: " + repr((x_status.CurPosition)))

    def go_move(self, distance):
        """
        Move to the specified coordinate.

        Depending on the mode parameter, you can set coordinates in steps or feedback counts, or in custom units.

        :param lib: structure for accessing the functionality of the libximc library.
        :param device_id: device id.
        :param distance: the position of the destination.
        :param mode:  mode in feedback counts or in user units. (Default value = 1)
        """

        print("\nMove to the position {0} specified in user units.".format(distance))
        result = lib.command_move_calb(self.device, c_float(distance), byref(self.user_unit))

    def go_shift(self, distance):
        """
        The shift by the specified offset coordinates.
        
        Depending on the mode parameter, you can set coordinates in steps or feedback counts, or in custom units.
        
        :param lib: structure for accessing the functionality of the libximc library.
        :param device_id: device id.
        :param distance: size of the offset in steps.
        :param mode:  (Default value = 1)
        """

        print("\nShift to the position {0} specified in user units.".format(distance))
        result = lib.command_movr_calb(self.device, c_float(distance), byref(self.user_unit))

    def wait_for_stop(self, interval=1):
        """
        Waiting for the movement to complete.

        :param lib: structure for accessing the functionality of the libximc library.
        :param device_id: device id.
        :param interval: step of the check time in milliseconds.
        """

        print("\nWaiting for stop")
        result = lib.command_wait_for_stop(self.device, interval)
        print("Result: " + repr(result))

    def set_user_unit_mode(self, fl_val):
        """
        User unit mode settings
        
        After setting this multiplier, you can use special commands with the suffix _calb to set the movement in mm or degrees.
        Follow the on-screen instructions to change the settings.
        
        :param lib: structure for accessing the functionality of the libximc library.
        :param device_id: device id.
        """

        print("\nUser unit mode settings.")
        try:
            self.user_unit.A = fl_val
            # user_unit.MicrostepMode the value is set together with eng.MicrostepMode
            print("User unit coordinate multiplier = {0} \n".format(self.user_unit.A))
        except:
            print("User unit coordinate multiplier = ", self.user_unit.A)

    def go_home(self):
        pass
        #lib.command_home(self.device)

    def go_emergencyStop(self):
        lib.command_stop(self.device)

    def go_stop(self):
        lib.command_sstp(self.device)

    def go_left(self):
        """
        Move to the left.

        :param lib: structure for accessing the functionality of the libximc library.
        :param device_id: device id.
        """

        print("\nMoving left")
        result = lib.command_left(self.device)

    def go_right(self):
        """
        Move to the right.

        :param lib: structure for accessing the functionality of the libximc library.
        :param device_id: device id.
        """

        print("\nMoving right")
        result = lib.command_right(self.device)

    def set_zero(self):
        lib.command_zero(self.device)

    @property
    def position(self):
        """
        Obtaining information about the position of the positioner.

        This function allows you to get information about the current positioner coordinates,
        both in steps and in encoder counts, if it is set.
        Also, depending on the state of the mode parameter, information can be obtained in user units.

        :param lib: structure for accessing the functionality of the libximc library.
        :param mode: mode in feedback counts or in user units. (Default value = 1)
        """

        # print("\nRead position")
        x_pos = get_position_calb_t()
        result = lib.get_position_calb(self.device, byref(x_pos), byref(self.user_unit))
        #if result == Result.Ok:
        #    pass
            # print("Position: {0} user unit".format(x_pos.Position), end="\r")
        return x_pos.Position

    @position.setter
    def position(self, position):

        x_pos = set_position_calb_t()
        x_pos.Position = c_float(position)
        x_pos.uPosition = c_longlong(0)
        x_pos.PosFlags = c_uint(0)

        lib.set_position_calb(self.device, byref(x_pos), byref(self.user_unit))



    # TODO: nastaveni klavesnici
    def microstep_mode(self):
        """
        Setting the microstep mode. Works only for stepper motors
        
        Follow the on-screen instructions to change the settings.
        
        :param lib: structure for accessing the functionality of the libximc library.
        :param device_id: device id.
        """

        print("\nMicrostep mode settings.")
        print("This setting is only available for stepper motors.")
        # Get current engine settings from controller
        result = lib.get_engine_settings(self.device, byref(self.engineParameters))
        if result == Result.Ok:
            # Current MICROSTEP_MODE
            Microstep_Mode = ["", "MICROSTEP_MODE_FULL", "MICROSTEP_MODE_FRAC_2", "MICROSTEP_MODE_FRAC_4",
                              "MICROSTEP_MODE_FRAC_8", "MICROSTEP_MODE_FRAC_16", "MICROSTEP_MODE_FRAC_32",
                              "MICROSTEP_MODE_FRAC_64", "MICROSTEP_MODE_FRAC_128", "MICROSTEP_MODE_FRAC_256"]
            print("The mode is set to", Microstep_Mode[self.engineParameters.MicrostepMode], "\n")
            # Change MicrostepMode parameter
            # (use MICROSTEP_MODE_FULL to MICROSTEP_MODE_FRAC_256 - 9 microstep modes)
            for range_val in range(len(Microstep_Mode)):
                if range_val > 0:
                    print("Set mode {0} - press {1}".format(Microstep_Mode[range_val], range_val))
            try:
                in_val = int(0)  # 0 je tu namisto get character
                if in_val > 0 and in_val <= 9:
                    self.engineParameters.MicrostepMode = in_val
                    self.user_unit.MicrostepMode = in_val
                print("The mode is set to", Microstep_Mode[self.engineParameters.MicrostepMode])
            except:
                print("The mode is set to", Microstep_Mode[self.engineParameters.MicrostepMode])
            result = lib.set_engine_settings(self.device, byref(self.engineParameters))
            if result != Result.Ok:
                print("Error recording microstep mode.")
            print("")


if __name__ == "__main__":
    lin = Standa("COM4")
    lin2 = Standa("COM22")
    lin.engineParameters.MicrostepMode = 9
    lin.user_unit.MicrostepMode = 9
    lin.set_user_unit_mode(1 / 200)
    lin.get_info()

    sys.exit()
