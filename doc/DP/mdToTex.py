from plantuml import PlantUML
import os
from os.path import abspath
import io
from svglib.svglib import svg2rlg
from reportlab.graphics import renderPDF
import re
import unidecode
import shutil, datetime

now=datetime.datetime.now()

shutil.copyfile("DP_text.md", "archive/DP_text_" + now.strftime("%Y%m%d_%H%M_%S,%f")[:-4]+".md")

with io.open("DP_text.md",encoding="utf-8") as file:
    f=file.read()

# create a server object to call for your computations
server = PlantUML(url='http://www.plantuml.com/plantuml/svg/',
                          basic_auth={},
                          form_auth={}, http_opts={}, request_opts={})
## Generate UML diagrams
while f.find("@startuml")>-1:
    umlstring=""
    umlstring_label=""
    umlstring_file=""
    umlstring_size="5"
    
    for l in f[f.find("@startuml"):f.find("@enduml")+len("@enduml")].splitlines():
        if l.find("@name")!=-1:
            umlstring_label=l.replace("@name:","").strip()
        elif l.find("@size")!=-1:
            umlstring_size=l.replace("@size:","").strip()
        else:
            umlstring=umlstring+"\n"+l

    umlstring_file=unidecode.unidecode(umlstring_label.replace(" ","_"))+".svg"
    
    umlstring_path="./img/uml/"+umlstring_file
    
    with open(umlstring_path,"wb") as fil:
        fil.write(server.processes(umlstring))
        
    drawing = svg2rlg(umlstring_path)
    umlstring_path=umlstring_path.replace(".svg",".pdf")
    renderPDF.drawToFile(drawing, umlstring_path.replace(".svg",umlstring_path))

    f=f[:f.find("@startuml")]+f"\medskip \n\picw={umlstring_size}cm \cinspic .{umlstring_path} \n\caption/f {umlstring_label} \n\medskip"+f[f.find("@enduml")+len("@enduml"):]



f=f.replace("\n#### ","\n\seccc ")
f=f.replace("\n### ","\n\secc ")
f=f.replace("\n## ","\n\sec ")
f=f.replace("\n# ","\n\chap ")

f= (re.sub(r'\n\n\-', r"\n\n\\begitems \n*", f))
f=(re.sub(r"\n- ",r"\n* ", f))
f=(re.sub(r"(\* .*)\n\n",r"\1\n\\enditems \n\n",f))


file = io.open("sablona/generated.tex","w",encoding="utf-8")
file.write(f)
file.close()

from subprocess import check_output
commit=check_output("git rev-list HEAD --count", shell=True).decode().strip()+", " + now.strftime("%m%d-%H%M-%S,%f")[:-4]

with io.open("sablona/commit.tex","w",encoding="utf-8") as file:
    file.write("\def\commit{"+str(commit)+"}")