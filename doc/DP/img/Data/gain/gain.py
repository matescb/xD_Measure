import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.signal import find_peaks
import glob
import numpy as np

def getFromMca(mca):
    f=open(mca,"r")
    d=f.read()
    f.close()

    data=[]
    for a in d.split("\n",32)[32].splitlines():
        data.append(int(a))
    return(data)

def gainFromMca(pathDemo,pathRef,rollingMean):
    ref=getFromMca(pathRef)
    demo=getFromMca(pathDemo)
    
    ref =np.array(pd.DataFrame(ref).rolling(rollingMean).mean().fillna(0))
    demo=np.array(pd.DataFrame(demo).rolling(rollingMean).mean().fillna(0))
    
    gain=[]
    for a in range(len(ref)):
        if ref[a]!=0:
            gain.append(demo[a]/ref[a])
        else:
            if demo[a]==0:
                gain.append(0)
            else:
                gain.append(0)
    return(np.nan_to_num(gain))
    

usable=[102,105,107]

target={102:"Al",105:"Ag",107:"Ti", 14:"Ti"}
ra=round(9/0.003662109375)   
co=0   

# Load mca data
gainAll={}
for u in usable:
    name=("gain_data\\Demo_823\\"+str(u)+".mca")
    gainAll[name]=gainFromMca(name,name.replace("Demo_823","Ref_742"),30)

# generate separate graphs for targets
for g in gainAll:
    gain=gainAll[g]
    gain[0:150]=0
    name=g.replace("gain_data\\Demo_823\\","").replace(".mca","")
    plt.plot(np.arange(0,(30-co),(30-co)/len(gain))[0:ra],gain[0:ra],label=target[int(name)])
    plt.grid()
    plt.legend()
    plt.ylabel("Gain [x]")
    plt.xlabel("Energie [keV]")
    plt.tight_layout()
    plt.savefig(f"{target[int(name)]}_{name}.png",format="png",dpi=400)
    plt.savefig(f"{target[int(name)]}_{name}.pdf",format="pdf")
    plt.cla()
    plt.close()
    
# generate data for axis
for g in gainAll:
    gain=gainAll[g]
    gain[0:150]=0
    name=g.replace("gain_data\\Demo_823\\","").replace(".mca","")
    plt.plot(np.arange(0,(30-co),(30-co)/len(gain))[0:ra],gain[0:ra],label=target[int(name)])

# generate mean of data
al=np.zeros(8192)
for g in gainAll:
    al=al+np.array(gainAll[g])
al=al/len(usable)
al[0:150]=0
plt.plot(np.arange(0,(30-co),(30-co)/len(gain))[0:ra],al[0:ra],label="Mean", color="Black")
    
# Save graph
plt.axhline(y=1, ls='-.',color="red",linewidth=1,alpha=0.75,label="Gain = 1")
plt.grid()
plt.legend()
plt.ylabel("Gain [x]")
plt.xlabel("Energie [keV]")
plt.tight_layout()
plt.savefig(f"all_{name}.png",format="png",dpi=400)
plt.savefig(f"all_{name}.pdf",format="pdf")