import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
from scipy.signal import find_peaks

mca=r"107.mca"
f=open(mca,"r")
d=f.read()
f.close()

sp=[]
for a in d.split("\n",32)[32].splitlines():
    sp.append(int(a))
    
ra=round(8/0.003662109375)
peaks, _=find_peaks(pd.DataFrame(sp[0:ra]).rolling(3).mean()[0], height=30,distance=100)

co=-0.6
plt.axvline(x=0.5416259765625, ls='--',color="purple", label=f"%.3f keV"% (0.5416259765625-0.06))

np.array(np.arange(0,(30-co),(30-co)/len(sp))[0:ra])[peaks]
for a, c in zip(np.array(np.arange(0,(30-co),(30-co)/len(sp))[0:ra])[peaks], ['r','b','g'], ):
    plt.axvline(x=a, ls='--',color=c, label=f"%.3f keV"% a)
    print(a)

plt.plot(np.arange(0,(30-co),(30-co)/len(sp))[0:ra],pd.DataFrame(sp[0:ra]).rolling(3).mean(),color="black", label="Ti anoda")
#plt.plot(np.array(np.arange(0,(30-co),(30-co)/len(sp))[0:ra])[peaks],np.array(pd.DataFrame(sp[0:ra]).rolling(3).mean()[0])[peaks], "x")


mca=r"106.mca"
f=open(mca,"r")
d=f.read()
f.close()

sp=[]
for a in d.split("\n",32)[32].splitlines():
    sp.append(int(a))
    
plt.plot(np.arange(0,(30-co),(30-co)/len(sp))[0:ra],pd.DataFrame(sp[0:ra]).rolling(10).mean(),color="orange",label=f"Ti anoda s\n20μm Ti filtrem")
    
plt.yscale("log")
plt.legend()
plt.grid()
plt.ylabel("Zásahy [-]")
plt.xlabel("Energie [keV]")

plt.savefig("107.mca.pdf",format="pdf")