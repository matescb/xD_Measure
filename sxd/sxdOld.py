
from xdSpectral.xdSpectralClass import xdSpectral
from xdCore import spm
import numpy as np
import matplotlib.pyplot as plt

if __name__ == "__main__":
    from sxdOldCom import SxdOld
else:
    from .sxdOldCom import SxdOld

from xdMeasurement import xdMeasurement
import os

class xdSxdOld(SxdOld, xdSpectral):
    def __init__(self, name):
        super(xdSxdOld,self).__init__(name)
        self._zeroBuffer()

    def __del__(self):
        super(xdSxdOld, self).__del__()

    def _zeroBuffer(self):
        self.buffer = list(np.zeros(self.bins, int))

    def _addBuffer(self, data):
        self.buffer=list(np.add(self.buffer, data))

    def open(self):
        super().open()

    def close(self):
        super().close()

    def start(self):
        super().start()
        data = self.measure
        del data
        self._zeroBuffer()

    def stop(self):
        super().stop()

    def resume(self):
        data = self.measure
        del data

    def read(self):
        self._addBuffer(self.measure)
        spm.addMeasured(xdMeasurement({"data":self.buffer, "binSize":self.binSize},self.save), self.name)

    def save(self, data:xdMeasurement, path):
        with open(os.path.join(path,f"SXD_{self.name}.txt"), 'w', encoding='utf-8') as f:
            f.write(str(data.data["data"]))

        plt.plot(np.arange(0, len(data.data["data"]) * data.data["binSize"], data.data["binSize"]), data.data["data"])
        plt.ylabel('Counts')
        plt.xlabel('keV')
        plt.savefig(os.path.join(path,f"SXD_{self.name}.png"), dpi=500)