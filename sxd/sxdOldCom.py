import numpy as np
import serial
import re
from time import sleep

class SxdOld:
    bins=512

    def __init__(self, *args, **kwargs) -> None:
        super(SxdOld, self).__init__(*args, **kwargs)

    def __del__(self):
        self.close()

    def open(self,COM="COM6"):
        self.ser = serial.Serial(COM, 115200, 8, "N", 1)  # open serial port

    def close(self):
        self.ser.close()

    @property
    def version(self):
        self.ser.write(b"I\n")
        sleep(0.05)
        return self.ser.read_all()

    @property
    def temperature(self):
        self.ser.write(b"T\n")
        sleep(0.05)
        temp = self.ser.read_all()
        return int(re.search("R:T ([0-9]*)", str(temp)).group(1)) / 10

    def reset(self):
        self.ser.write(b"Q\n")

    @property
    def measure(self):
        self.ser.write(b"C\n")
        sleep(0.1)
        mes = self.ser.readline()
        mes = mes.decode().rstrip("\n")

        parts = re.search("R:([IFTMSC])([0-9]*)\\|([0-9A-F]*)\\|([0-9A-F]*)\\|([0-9A-Fr]*)", mes)

        hist_bits = int(parts.group(2), 16)
        value_nibbles = int(parts.group(3), 16)
        points = int(parts.group(4), 16)
        data = parts.group(5)

        rv = []
        pos = 0
        p_count = 0
        while pos + value_nibbles <= len(data):
            repeat = 1
            if data[pos] == "r":
                #    if (pos + 3 + nibblePerValue > text.size()) { qCritical() << "Format error!";
                repeat = int(data[pos + 1: pos + 1 + 2], 16)  # .toUInt(nullptr, 16));
                pos += 3;

            value = int(data[pos:pos + value_nibbles], 16)

            for g in range(repeat):
                rv.append(value)
            p_count = p_count + value * repeat

            pos = pos + value_nibbles

#        print(hist_bits, points, p_count)
        return rv


if __name__ == "__main__":
    sxd = SxdOld()
    sxd.open()
    hist = sxd.measure
   # print(plotille.plot(range(0,len(hist)), hist))
    sxd.close()
    del sxd
