from PyQt6.QtWidgets import QApplication, QWidget, QPushButton, QLabel, QDialog, QFrame, QSizePolicy, QFileDialog
from PyQt6 import QtCore, QtWidgets
import sys, os

from mainWindow_ui import Ui_MainWindow
from PyQt6.QtWidgets import QMainWindow
from xdCore import stg, cmg, spm, saveMeasured, cfg, cfgSave
from xdCam.xdCameraWidget  import CameraWidget
from sxd.sxdOld import xdSxdOld

from hamamatsu.hamamatsu import xdHamamatsu
from ketek.ketek import xdKetek
from xdSpectral.xdSpectralWidget import SpectralWidget
from timepix.timepix import xdTimepix
from timepix.timepix_quad import xdTimepixQuad

from standa.widget.standaWidget import standaWidget

from toolkit.scanner.scannerWidget import ToolScanner

from toolkit.scanner2D.scanner2DWidget import ToolScanner2D
from toolkit.scannerHamamatsu.scannerHamamatsuWidget import ToolScannerHamamatsu

from toolkit.demo.demoWidget import ToolDemo

'''
D:\DP\QT_playing\venv\Scripts\pyuic6.exe D:\DP\QT_playing\mainWindow.ui -o D:\DP\QT_playing\mainWindow_ui.py
'''
class UI(Ui_MainWindow, QMainWindow):
    _stageframes = {}

    def __init__(self):
        super(UI, self).__init__()
        self.tools={}
        self.setupUi(self)
        self.pushButton_stageAdd.clicked.connect(self.openStageMenu)
        self.comboBox_stageSelect.hide()
        self.pushButton_stageConfirm.hide()
        self.pushButton_stageReset.hide()

        self.pushButton_stageReset.clicked.connect(self.closeStageMenu)
        self.pushButton_stageConfirm.clicked.connect(self.selectStageUpdater)

        self.pushButton_emergencyStop.clicked.connect(stg.go_emergencyStopAll)
        self.pushButton_allZero.clicked.connect(stg.go_zeroAll)
        self.pushButton_allHome.clicked.connect(stg.go_homeAll)

        self.actionKetek.triggered.connect(self.addKetek)
        self.actionKetek_2.triggered.connect(self.addKetek2)

        self.actionHamamatsu_2.triggered.connect(self.addHamamatsu)
        self.actionSXD_old_FW.triggered.connect(self.addSxdOld)
        self.actionTimepix.triggered.connect(self.addTimepix)

        self.actionQUAD.triggered.connect(self.addQUAD)
        self.actionQUAD_New.triggered.connect(self.addQUADNew)

        self.pushButton_saveAll.clicked.connect(self.saveMeasured)
        self.pushButton_measureAll.clicked.connect(self.measureAll)

        self.actionScanner.triggered.connect(lambda: self.openTool(ToolScanner(self)))
        self.actionDemo_pro_DP_prezentaci.triggered.connect(lambda: self.openTool(ToolDemo(self)))
        self.actionScanner_2D.triggered.connect(lambda: self.openTool(ToolScanner2D(self)))
        self.actionScanner_Hamamatsu.triggered.connect(lambda: self.openTool(ToolScannerHamamatsu(self)))


        self.spinBox.valueChanged.connect(self.spectralValueChange)
        self.spinBox.valueChanged.connect(self.spectralValueChange)

        self.actionSetProjectFolder.triggered.connect(self.setProjectFolder)

        if cfg["Project"]["projectFolder"] == "":
            self.setProjectFolder()
        else:
            self.setWindowTitle("XD Measure" + " -- " + cfg["Project"]["projectFolder"])


    def setProjectFolder(self):
        dialog=QFileDialog

        path=os.path.dirname(os.path.abspath(__file__))
        if cfg.has_option("Project", "projectFolder"):
            if cfg["Project"]["projectFolder"]!="":
                path=cfg["Project"]["projectFolder"]

        path=dialog.getExistingDirectory(None,"Select Folder")
        cfg.set("Project", "projectFolder", path)

        cfgSave()
        print(path)

        self.setWindowTitle("XD Measure" + " -- " + path)

    def spectralValueChange(self):
        spm.measureTime=int(self.spinBox.value())

    def openTool(self,fce):
        self.tools[fce.objectName()]=fce
        self.tools[fce.objectName()].show()

    def measureAll(self):
        spm.measureAll()
        cmg.measureAll()

    def saveMeasured(self):
        saveMeasured(scanName=str(self.lineEdit_saveName.text()))

    def addKetek(self):
        spm.openDevice(xdKetek("Ketek","KETEK_DPP2_usb2.ini",0))
        window.ketekFrame = SpectralWidget(window.centralwidget, "Ketek")
        window.ketekFrame.setObjectName("ketekFrame")
        window.tab_ketek = (window.ketekFrame)
        window.tabWidget.addTab(window.tab_ketek, "Ketek")

    def addKetek2(self):
        spm.openDevice(xdKetek("Ketek2","KETEK_DPP2_usb2_2.ini",1))
        window.ketekFrame = SpectralWidget(window.centralwidget, "Ketek2")
        window.ketekFrame.setObjectName("ketekFrame")
        window.tab_ketek2 = (window.ketekFrame)
        window.tabWidget.addTab(window.tab_ketek2, "Ketek2")

    def addHamamatsu(self):
        cmg.openCamera(xdHamamatsu("Hamamatsu"))
        window.hamamatsuFrame = CameraWidget(window.centralwidget, "Hamamatsu")
        window.hamamatsuFrame.setGeometry(QtCore.QRect(700, 70, 571, 721))
        window.hamamatsuFrame.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        window.hamamatsuFrame.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        window.hamamatsuFrame.setObjectName("hamamatsuFrame")
        window.tab_hamamatsu = (window.hamamatsuFrame)
        window.tabWidget.addTab(window.tab_hamamatsu, "Hamamatsu")
        self.actionHamamatsu_2.setEnabled(False)

    def addTimepix(self):
        cmg.openCamera(xdTimepix("timepix"))
        window.hamamatsuFrame = CameraWidget(window.centralwidget, "timepix")
        window.hamamatsuFrame.setGeometry(QtCore.QRect(700, 70, 571, 721))
        window.hamamatsuFrame.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        window.hamamatsuFrame.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        window.hamamatsuFrame.setObjectName("timepixFrame")
        window.tab_timepix = (window.hamamatsuFrame)
        window.tabWidget.addTab(window.tab_timepix, "timepix")

    def addQUAD(self):
        cmg.openCamera(xdTimepixQuad("QUAD_Ref",{0: '00', 1: 'I06', 2: 'C10', 3: 'K05', 4: 'J10'}))
        window.hamamatsuFrame = CameraWidget(window.centralwidget, "QUAD_Ref")
        window.hamamatsuFrame.setGeometry(QtCore.QRect(700, 70, 571, 721))
        window.hamamatsuFrame.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        window.hamamatsuFrame.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        window.hamamatsuFrame.setObjectName("QUADFrame")
        window.tab_quad= (window.hamamatsuFrame)
        window.tabWidget.addTab(window.tab_quad, "QUAD Ref")

    def addQUADNew(self):
        cmg.openCamera(xdTimepixQuad("QUAD_Demo",{0: '00', 1: 'C09', 2: 'G11', 3: 'J08', 4: 'G10'}))
        window.hamamatsuFrame = CameraWidget(window.centralwidget, "QUAD_Demo")
        window.hamamatsuFrame.setGeometry(QtCore.QRect(700, 70, 571, 721))
        window.hamamatsuFrame.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        window.hamamatsuFrame.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        window.hamamatsuFrame.setObjectName("QUADFrame")
        window.tab_quad = (window.hamamatsuFrame)
        window.tabWidget.addTab(window.tab_quad, "QUAD demo")

    def addSxdOld(self):
        spm.openDevice(xdSxdOld("SXD_oldFW"))
        window.sxdOldFrame = SpectralWidget(window.centralwidget, "SXD_oldFW")
        window.sxdOldFrame.setGeometry(QtCore.QRect(700, 70, 571, 721))
        window.sxdOldFrame.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        window.sxdOldFrame.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        window.sxdOldFrame.setObjectName("sxdOldFrame")
        window.sxdOldFrame = (window.sxdOldFrame)
        window.tabWidget.addTab(window.sxdOldFrame, "SXD (old FW)")

    def selectStageUpdater(self):
        self.addStage(self.comboBox_stageSelect.currentText())
        self.closeStageMenu()

    def openStageMenu(self):
        self.comboBox_stageSelect.clear()
        self.comboBox_stageSelect.addItems(list(stg.stagesAll))

        self.pushButton_stageAdd.hide()
        self.comboBox_stageSelect.show()
        self.pushButton_stageConfirm.show()
        self.pushButton_stageReset.show()

    def closeStageMenu(self):
        self.comboBox_stageSelect.clear()
        self.comboBox_stageSelect.addItems(list(stg.stagesAll))

        self.pushButton_stageAdd.show()
        self.comboBox_stageSelect.hide()
        self.pushButton_stageConfirm.hide()
        self.pushButton_stageReset.hide()

    def addStage(self, stage):
        if stg.tryName(stage):
            self._stageframes[stage] = standaWidget(self.verticalLayout_stages, stage)
            self._stageframes[stage].setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
            self._stageframes[stage].setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
            self.verticalLayout_stages.addWidget(self._stageframes[stage])
            self.show()
            #self.frame.close()

if __name__ == "__main__":
    app = QApplication([])
    window = UI()

#    window.addStage("HamamatsuStage")
 #   window.addHamamatsu()

    window.show()
  #  window.openDemo()

    sys.exit(app.exec())
