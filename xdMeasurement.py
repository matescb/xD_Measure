
from datetime import datetime
from xdCore import stg
import jsonpickle
from astropy.io import fits
import numpy as np
class xdMeasurement():

    notes = ""
    device = ""

    def __init__(self, data, saveFce) -> None:
        self.time = datetime.now()
        self.position = stg.positions
        self.data = data
        self.latest = True
        self.saveFce=saveFce

    def __str__(self) -> str:
        return f"time:{self.time}, position: {self.position}, device: {self.device}"

    def save(self, path, name):
      #  with open(f"{path}/{name}_dump.json", "w") as text_file:
       #     text_file.write(jsonpickle.encode(self))

        with open(f"{path}/{name}_metadata.txt", "w") as text_file:
            text_file.write(str(self))

        self.saveFce(self, path)

        try:
            hdu=None
            if len(np.array(self.data["data"]).shape)==1:
                hdu=fits.PrimaryHDU((np.array(self.data["data"])).reshape(1, len(self.data["data"])))
            else:
                hdu = fits.PrimaryHDU(self.data["data"].astype(int))
            hdu.header['TIME'] = str(self.time)
            hdu.header['COMMENT'] = "Standa stage positions."
            for a in self.position:
                hdu.header["STAGE"]=a["stage"]
                hdu.header["POSITION"]=a["position"]
            hdu.writeto(f"{path}/{name}.fits", overwrite=True)
        except Exception as e: print(e)





